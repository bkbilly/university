#!/usr/bin/env python3
# coding: utf-8

import sys
import time
import pybullet as p
from qibullet import SimulationManager
from qibullet import PepperVirtual
from qibullet import NaoVirtual
from qibullet import RomeoVirtual

if __name__ == "__main__":
    simulation_manager = SimulationManager()

    rob = 'pepper'
    client = simulation_manager.launchSimulation(gui=True)
    robot = simulation_manager.spawnPepper(client, spawn_ground_plane=True)
    # if (sys.version_info > (3, 0)):
    #     rob = input("Which robot should be spawned? (pepper/nao/romeo): ")
    # else:
    #     rob = raw_input("Which robot should be spawned? (pepper/nao/romeo): ")

    # time.sleep(1.0)
    joint_parameters = list()

    robot.setAngles(["LShoulderPitch", "LShoulderRoll", "LElbowYaw", "LElbowRoll"], [0.22, 0, -0.5, -1], [0.5, 0.5, 0.5, 0.5])
    time.sleep(1.0)
    # import ipdb
    # ipdb.set_trace()
    joints = robot.joint_dict.items()
    joints = sorted(joints, key=lambda tup: tup[0])
    for name, joint in joints:
        print(name)
        if "Hand" not in name and "Thumb" not in name:
            joint_parameters.append((
                p.addUserDebugParameter(
                    name,
                    joint.getLowerLimit(),
                    joint.getUpperLimit(),
                    robot.getAnglesPosition(name)),
                name))

    while True:
        # robot.setAngles(["LFinger11", "LFinger12", "LFinger12"], [0, 0, 0], [0, 0, 0])
        # time.sleep(4)
        # robot.setAngles(["LFinger11", "LFinger12", "LFinger12"], [1, 1, 1], [1, 1, 1])
        time.sleep(4)
        for joint_parameter in joint_parameters:
            robot.setAngles(
                joint_parameter[1],
                p.readUserDebugParameter(joint_parameter[0]), 1.0)

