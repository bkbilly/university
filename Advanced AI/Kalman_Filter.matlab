x0 = [14 0]'
Q = [0.1^2 0; 0 0.2^2]
F = [1 0; 0.05 1]
H = [0 1]
R = [0.5^2]
P0 = Q
z1 = 1.1499

xp1 = F * x0
Pp1 = F * P0 * F' + Q

S = H * Pp1 * H' + R
K1 = Pp1 * H' * inv(S)

x1 = xp1 + K1 * (z1 - H * xp1)
P1 = Pp1 - K1 * S * K1'
