#!/usr/bin/env python3

import random
from bayesian import ParseInputs
import logging
from datetime import datetime
# import ipdb1


logger = logging.getLogger()
logger.addHandler(logging.StreamHandler())

class ApproximateInference():
    """docstring for ApproximateInference"""
    def __init__(self):
        pass

    def run(self, testcase):
        self.samples = testcase['samples']
        # Initialize graph
        parser = ParseInputs()
        known_data = parser.get_test_data(testcase['netid'])
        self.graph = parser.get_graph(known_data)

        node, query = parser.get_query('|'.join(testcase['query']))

        # Test with my data graph
        timer_start = datetime.now()
        likelihood = self.start_inference(node, query)
        normalized = self.normalize(likelihood)
        print '--> LikelihoodWeighting:', likelihood
        print '  Normalized:', normalized
        percWrong = {}
        for key in normalized.keys():
            percWrong[key] = round(abs(normalized[key] - testcase['result'][key]) * 100, 2)
        print '  percent Wrong:', percWrong
        print '  time:', datetime.now() - timer_start


    def get_probability(self, node, conditions):
        prob = self.graph[node]['prob']
        if prob is None:
            conditional = []
            for par in self.graph[node]['parents']:
                conditional.append(conditions[par]['value'])
            conditional = tuple(conditional)
            prob = self.graph[node]['condition'][conditional]

        return prob

    def random_sample(self, node, prob):
        randnumber = random.random()  # between 0 and 1

        retNode = True
        if randnumber > prob:
            retNode = False

        return retNode

    def normalize(self, prob_results):
        normalized = {}
        mysum = float(sum(prob_results.values()))
        for cond, value in prob_results.items():
            if mysum != 0:
                normalized[cond] = round(value / mysum, 3)
            else:
                normalized[cond] = 0
        return normalized


class PriorSampling:
    CPTs = {}

    def __init__(self):
        # self.initialiseNet(netID)
        pass
        # print(self.CPTs)

    def initialiseNet(self, netID):
        if netID == "burglary":
            self.CPTs["B"] = {"+b": 0.001, "-b": 0.999}
            self.CPTs["E"] = {"+e": 0.002, "-e": 0.998}
            self.CPTs["A"] = {"+a|+b+e": 0.95, "-a|+b+e": 0.05,
                              "+a|+b-e": 0.94, "-a|+b-e": 0.06,
                              "+a|-b+e": 0.29, "-a|-b+e": 0.71,
                              "+a|-b-e": 0.001, "-a|-b-e": 0.999}
            self.CPTs["J"] = {"+j|+a": 0.90, "-j|+a": 0.10,
                              "+j|-a": 0.05, "-j|-a": 0.95}
            self.CPTs["M"] = {"+m|+a": 0.70, "-m|+a": 0.30,
                              "+m|-a": 0.01, "-m|-a": 0.99}
            self.CPTs["order"] = ["B", "E", "A", "J", "M"]
            self.CPTs["parents"] = {"B": None,
                                    "E": None, "A": "B,E", "J": "A", "M": "A"}

        elif netID == "sprinkler":
            self.CPTs["C"] = {"+c": 0.50, "-c": 0.50}
            self.CPTs["S"] = {"+s|+c": 0.10, "-s|+c": 0.90,
                              "+s|-c": 0.50, "-s|-c": 0.50}
            self.CPTs["R"] = {"+r|+c": 0.80, "-r|+c": 0.20,
                              "+r|-c": 0.20, "-r|-c": 0.80}
            self.CPTs["W"] = {"+w|+s+r": 0.99, "-w|+s+r": 0.01,
                              "+w|+s-r": 0.90, "-w|+s-r": 0.10,
                              "+w|-s+r": 0.90, "-w|-s+r": 0.10,
                              "+w|-s-r": 0.00, "-w|-s-r": 1.00}
            self.CPTs["order"] = ["C", "S", "R", "W"]
            self.CPTs["parents"] = {"C": None, "S": "C", "R": "C", "W": "S,R"}

        else:
            print("UNKNOWN network=" + str(netID))
            exit(0)
        return self.CPTs

    def random_sample(self, CPT, conditional):
        # print(CPT, conditional)
        sampledValue = None
        randnumber = random.random()

        value1 = CPT["+" + conditional]
        value2 = CPT["-" + conditional]
        # print(value1, randnumber)

        if randnumber <= value1:
            sampledValue = "+" + conditional
        else:
            sampledValue = "-" + conditional

        # print(sampledValue.split("|")[0])
        return sampledValue.split("|")[0]

    def sampleVariables(self, printEvent):
        # print('----------============-----------')
        event = []
        sampledVars = {}

        for variable in self.CPTs["order"]:
            evidence = ""
            conditional = ""
            parents = self.CPTs["parents"][variable]
            if parents is None:
                conditional = variable.lower()
            else:
                for parent in parents.split(","):
                    evidence += sampledVars[parent]
                conditional = variable.lower() + "|" + evidence

            sampledValue = self.random_sample(self.CPTs[variable], conditional)
            event.append(sampledValue)
            sampledVars[variable] = sampledValue
            # print '===='
            # print 'sampledVars:', sampledVars
            # print 'self.CPTs[' + variable + ']:', self.CPTs[variable]
            # print 'conditional:', conditional
            # print 'evidence:', evidence
            # print 'parents:', parents
            # print 'prob:', self.CPTs[variable]['+' + conditional]
            # print 'event:', event
            # print 'sampledValue:', sampledValue
            # print '\n'

        # if printEvent:
        #     print(event)
        return event


class Rejection():
    """docstring for Rejection"""
    CPTs = {}

    def __init__(self, samples, dataset=None):
        self.samples = samples
        self.ps = PriorSampling()
        if dataset is not None:
            self.CPTs = self.ps.initialiseNet(dataset)
        # print(self.ps.CPTs)

    def normalize(self, prob_results):
        normalized = {}
        mysum = float(sum(prob_results.values()))
        for cond, value in prob_results.items():
            if mysum != 0:
                normalized[cond] = round(value / mysum, 3)
            else:
                normalized[cond] = 0
        return normalized

    def rejection_sampling(self, node, variables):
        N = {False: 0, True: 0}
        for num, sample in enumerate(range(self.samples)):
            sampVars = self.ps.sampleVariables(False)
            exists = any(val in sampVars for val in variables.split(','))
            if exists:
                # print(num, sampVars)
                if '-{0}'.format(node.lower()) in sampVars:
                    N[False] += 1
                if '+{0}'.format(node.lower()) in sampVars:
                    N[True] += 1
        # print('N =', N)
        return N


class RejectionSampling(ApproximateInference):

    def __init__(self, graph=None, samples=None):
        self.graph = graph
        self.samples = samples
        if self.samples <= 5 and self.samples is not None:
            logger.setLevel(logging.DEBUG)

    def start_inference(self, node, query):
        ''' rejection_sampling '''
        likelihood = {False: 0, True: 0}
        for num, sample in enumerate(range(self.samples)):
            sampVars = self.prior_sampling(query)
            for q_node, q_data in query.items():
                if sampVars[q_node]['value'] == q_data:
                    likelihood[True] += 1
                else:
                    likelihood[False] += 1

        return likelihood

    def prior_sampling(self, query):
        logger.debug('----------============-----------')
        conditions = {key: {'value': None, 'prob': 1} for key in self.graph.keys()}

        for node, node_options in self.graph.items():
            prob = self.get_probability(node, conditions)
            rand = self.random_sample(node, prob)
            conditions[node]['value'] = rand
            conditions[node]['prob'] = prob

        logger.debug('\n')
        logger.debug('conditions: ' + str(conditions))
        logger.debug('\n')

        return conditions


class LikelihoodWeighting(ApproximateInference):

    def __init__(self, graph=None, samples=None):
        self.graph = graph
        self.samples = samples
        if self.samples <= 5 and self.samples is not None:
            logger.setLevel(logging.DEBUG)

    def start_inference(self, node, query):
        ''' weighted_likelihood '''
        likelihood = {False: 0, True: 0}
        for num, sample in enumerate(range(self.samples)):
            event, weight = self.weighted_sample(query)
            likelihood[event[node]['value']] += weight

        return likelihood

    def weighted_sample(self, query):
        logger.debug('----------============-----------')
        conditions = {key: {'value': None, 'prob': 1} for key in self.graph.keys()}

        for node, node_options in self.graph.items():
            logger.debug('--==' + node + '==--')
            if node in query:
                logger.debug(" exists in query")
                prob = self.get_probability(node, conditions)
                conditions[node]['value'] = query[node]
                conditions[node]['prob'] = prob
            else:
                logger.debug(' conditional')
                prob = self.get_probability(node, conditions)
                rand = self.random_sample(node, prob)
                conditions[node]['value'] = rand

        weight = 1
        for node, condition in conditions.items():
            if condition['prob'] != 1:
                if not condition['value']:
                    condition['prob'] = 1 - condition['prob']
                weight *= condition['prob']

        logger.debug('\n')
        logger.debug('conditions: ' + str(conditions))
        logger.debug('weight: ' + str(weight))
        logger.debug('\n')

        return conditions, weight


if __name__ == "__main__":
    samples = 10000

    testcases = [
        # {
        #     'netid': "sprinkler",
        #     'query': ('W', '-s,+r'),
        #     'result': {True: 0.90, False: 0.10},
        #     'samples': 10000,
        # },
        # {
        #     'netid': "sprinkler",
        #     'query': ('R', '-c,+w'),
        #     'result': {True: 0.35, False: 0.65},
        #     'samples': 10000,
        # },
        # {
        #     'netid': "disease",
        #     'query': ('D', '+t'),
        #     'result': {True: 0.35, False: 0.65},
        #     'samples': 10000,
        # },
        {
            'netid': "cancer",
            'query': ('S', '+co,+f'),
            'result': {True: 0.8, False: 0.2},
            'samples': 1000,
        },
    ]

    for testcase in testcases:
        RejectionSampling().run(testcase)
        LikelihoodWeighting().run(testcase)
    # for testcase in testcases:
    #     # Initialize graph
    #     parser = ParseInputs()
    #     known_data = parser.get_test_data(testcase['netid'])
    #     # print known_data
    #     graph = parser.get_graph(known_data)
    #     node, query = parser.get_query('|'.join(testcase['query']))

    #     timer_start = datetime.now()
    #     rejsampl = RejectionSampling(graph, samples)
    #     result = rejsampl.rejection_sampling(node, query)
    #     result2 = rejsampl.normalize(result)
    #     print '--> RejectionSampling:', result
    #     print '  Normalized:', result2
    #     percWrong = {}
    #     for key in result2.keys():
    #         percWrong[key] = round(abs(result2[key] - testcase['result'][key]) * 100, 2)
    #     print '  percent Wrong:', percWrong
    #     print '  time:', datetime.now() - timer_start


    #     # print '\n ---=== TestCase P({}) ===---'.format('|'.join(testcase['query']))
    #     # rs = Rejection(samples, testcase['netid'])
    #     # timer_start = datetime.now()
    #     # result = rs.rejection_sampling(*testcase['query'])
    #     # result2 = rs.normalize(result)
    #     # print '--> Rejection:', result
    #     # print '  Normalized:', result2
    #     # percWrong = {}
    #     # for key in result2.keys():
    #     #     percWrong[key] = round(abs(result2[key] - testcase['result'][key]) * 100, 2)
    #     # print '  percent Wrong:', percWrong
    #     # print '  time:', datetime.now() - timer_start

    #     # Test with my data graph
    #     timer_start = datetime.now()
    #     weighted = LikelihoodWeighting(graph, samples)
    #     result = weighted.weighted_likelihood(node, query)
    #     result2 = weighted.normalize(result)
    #     print '--> LikelihoodWeighting:', result
    #     print '  Normalized:', result2
    #     percWrong = {}
    #     for key in result2.keys():
    #         percWrong[key] = round(abs(result2[key] - testcase['result'][key]) * 100, 2)
    #     print '  percent Wrong:', percWrong
    #     print '  time:', datetime.now() - timer_start
