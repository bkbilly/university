#!/usr/bin/env python3

from collections import OrderedDict
import sys
import logging
import numpy as np

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
logger.addHandler(logging.StreamHandler())

transitions = {
    'feelings': ['COLD', 'WARM', 'HOT', 'WARM', 'COLD'],
    'hidden_states': ['ON', 'OFF'],
    'observations': ['HOT', 'WARM', 'COLD'],
    'ON': {
        'init': 1.0 / 2,
        'trans': {
            'ON': 0.7,
            'OFF': 0.3,
            'HOT': 0.4,
            'WARM': 0.4,
            'COLD': 0.2,
        },
    },
    'OFF': {
        'init': 1.0 / 2,
        'trans': {
            'ON': 0.3,
            'OFF': 0.7,
            'HOT': 0.1,
            'WARM': 0.45,
            'COLD': 0.45,
        },
    },
}


# Check if everything on config is ok
for hidden_state in transitions['hidden_states']:
    trans = (list(transitions[hidden_state]['trans'].keys()))
    checks = []
    checks.append(all(elem in trans for elem in transitions['hidden_states']))
    checks.append(all(elem in trans for elem in transitions['observations']))
    checks.append(all(elem in transitions['observations'] for elem in transitions['feelings']))
    checks.append(all(elem in transitions for elem in transitions['hidden_states']))
    if not all(checks):
        logger.debug("ERROR WITH THE DATA")
        sys.exit()


# Define previous probabilities for the 1st loop (this will be discarded)
path = [[transitions[hidden_state]['init'] for hidden_state in transitions['hidden_states']]]

# Calculate probabilities for all the hidden states through the observations
logger.debug('\n-- Calculate most likely path')
for num, feel in enumerate(transitions['feelings']):
    # A dictionary of hidden states that contains a list of probabilities
    probs_max = OrderedDict()
    for hidden_state in transitions['hidden_states']:
        for tmp_hidden in transitions['hidden_states']:
            yesterday_probs = dict(zip(transitions['hidden_states'], path[-1]))
            p1 = transitions[hidden_state]['trans'][feel]
            p2 = transitions[hidden_state]['trans'][tmp_hidden]
            p3 = yesterday_probs[tmp_hidden]

            reslt = p1 * p3 * p2
            logger.debug('  {} - {}: {} * {} * {} = {}'.format(
                hidden_state,
                tmp_hidden,
                p1,
                p2,
                round(p3, 3),
                round(reslt, 3),
            ))
            probs_max.setdefault(hidden_state, []).append(reslt)

    # From each hidden state probabilities get the maximum value
    logger.debug('  {}'.format(dict(probs_max)))
    probs_fixed = OrderedDict()
    prob_sum = 0
    for key, probs in probs_max.items():
        probs_fixed[key] = max(probs)
        prob_sum += max(probs)

    # If it's the 1st loop, then remove it from path and do a normalization
    if num == 0:
        path = []
        for key, prob in probs_fixed.items():
            probs_fixed[key] = prob / prob_sum

    path.append([prob for prob in probs_fixed.values()])
    logger.debug('{}, {}'.format(feel, dict(probs_fixed)))

# Calculate the probability and most likely path
logger.debug('\n-- Most likely path probabilities')
prob = 1
most_likely_path = []
for num, hidden_probabilities in enumerate(path, start=1):
    likelihood = dict(zip(transitions['hidden_states'], hidden_probabilities))
    most_likely = max(likelihood, key=likelihood.get)
    most_likely_path.append(most_likely)
    logger.debug('{}) {}: {}'.format(num, most_likely, likelihood))
    prob *= likelihood[most_likely]


# Calculate the probability path of observations
logger.debug('\n-- Observations path probabilities:')
observations_prob = 0
init_states = []
T_init = []
for state in transitions['hidden_states']:
    init_states.append(transitions[state]['init'])
    trans = []
    for tmp_state in transitions['hidden_states']:
        trans.append(transitions[state]['trans'][tmp_state])
    T_init.append(trans)
T = np.concatenate([T_init])

result = np.array(init_states).transpose()
for num, feeling in enumerate(transitions['feelings'], start=1):
    mylist = []
    for state in transitions['hidden_states']:
        mylist.append(transitions[state]['trans'][feeling])
    result = np.diag(np.array(mylist)).dot(T.transpose()).dot(result)
    logger.debug('{}) {}'.format(num, result))
observations_prob = np.sum(result)

logger.debug('\n-- Results:')
logger.info('Probability of observations path: {0}'.format(observations_prob))
logger.info('most likely path: {0}'.format(most_likely_path))
logger.info('most likely probability: {0:.10f}'.format(prob))
