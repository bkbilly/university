#!/usr/bin/env python3

# from numpy import random
# Transition Probabilities
p_nn = 0.7
p_nf = 0.3
p_fn = 0.3
p_ff = 0.7

# Initial Probabilities
p_n = 1. / 2
p_f = 1. / 2

# Emission Probabilities
p_nh = 0.4
p_nw = 0.4
p_nc = 0.2
p_fh = 0.1
p_fw = 0.45
p_fc = 0.45

feels = ['C', 'W', 'H', 'W', 'C']

probabilities = []
fan = []

if feels[0] == 'H':
    probabilities.append((p_n * p_nh, p_f * p_fh))
elif feels[0] == 'W':
    probabilities.append((p_n * p_nw, p_f * p_fw))
elif feels[0] == 'C':
    probabilities.append((p_n * p_nc, p_f * p_fc))


for feel in feels[1:]:
    yesterday_on, yesterday_off = probabilities[-1]
    if feel == 'H':
        today_on = max(yesterday_on * p_nn * p_nh,
                       yesterday_off * p_fn * p_nh)
        today_off = max(yesterday_on * p_nf * p_fh,
                        yesterday_off * p_ff * p_fh)
        probabilities.append((today_on, today_off))
    elif feel == 'W':
        today_on = max(yesterday_on * p_nn * p_nw,
                       yesterday_off * p_fn * p_nw)
        today_off = max(yesterday_on * p_nf * p_fw,
                        yesterday_off * p_ff * p_fw)
        probabilities.append((today_on, today_off))
    elif feel == 'C':
        today_on = max(yesterday_on * p_nn * p_nc,
                       yesterday_off * p_fn * p_nc)
        today_off = max(yesterday_on * p_nf * p_fc,
                        yesterday_off * p_ff * p_fc)
        probabilities.append((today_on, today_off))


prob = 1
for p in probabilities:
    if p[0] > p[1]:
        prob *= p[0]
        fan.append('ON')
    else:
        prob *= p[1]
        fan.append('OFF')


print(probabilities)
print(fan)
print(prob)
