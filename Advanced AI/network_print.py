import matplotlib.pyplot as plt
import networkx as nx
from bayesian import BayesianClass, ParseInputs

G = nx.DiGraph()

parser = ParseInputs()
known_data = parser.get_test_data('cancer')
graph = parser.get_graph(known_data)
for node, data in graph.items():
    G.add_node(node, prob=data['prob'])
    print node, data
    for child in data['children']:
        G.add_edge(node, child, condition=data['condition'])

# nx.draw(G, with_labels=True, font_weight='bold', node_size=500)
# nx.draw_circular(G, with_labels=True, font_weight='bold', node_size=500)
# nx.draw_shell(G, with_labels=True, font_weight='bold', node_size=500)
# nx.draw_networkx(G, with_labels=True, font_weight='bold', node_size=500)
nx.draw_spring(G, with_labels=True, font_weight='bold', node_size=500)

plt.show()
