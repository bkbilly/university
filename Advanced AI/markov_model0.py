#!/usr/bin/env python3

import numpy as np

# P(xt| e1,e2,..., et)
# at * P(et | xt) xt-1 [P(xt| xt-1) ft-1] = FORWARD(ft-1, et)
# ft = at * Ot * T^t * ft-1
transitions = {
    'observations': ['COLD', 'WARM', 'HOT', 'WARM', 'COLD'],
    'hidden_states': ['ON', 'OFF'],
    'ON': {
        'init': 1.0 / 2,
        'trans': {
            'ON': 0.7,
            'OFF': 0.3,
            'HOT': 0.4,
            'WARM': 0.4,
            'COLD': 0.2,
        },
    },
    'OFF': {
        'init': 1.0 / 2,
        'trans': {
            'ON': 0.3,
            'OFF': 0.7,
            'HOT': 0.1,
            'WARM': 0.45,
            'COLD': 0.45,
        },
    },
}

init_states = []
T_init = []
for state in transitions['hidden_states']:
    init_states.append(transitions[state]['init'])
    trans = []
    for tmp_state in transitions['hidden_states']:
        trans.append(transitions[state]['trans'][tmp_state])
    T_init.append(trans)
transition = np.concatenate([T_init]).transpose()

result = np.array(init_states).transpose()

for observation in transitions['observations']:
    observed = []
    print(observation)
    for num, state in enumerate(transitions['hidden_states']):
        observed.append(transitions[state]['trans'][observation])
        # print(transition)
        # semiresult = transition[num][0] * result[0] + transition[num][1] * result[1]
        # print('semiresult = {} * {} + {} * {} = {}'.format(
        #     transition[num][0], result[0], transition[num][1], result[1], semiresult))
        # print(semiresult)
        # print('{}: {} * ({}*{}+{}*{}) = {}'.format(
        #     state,
        #     observed[num],
        #     transition[num][0], result[0], transition[num][1], result[1],
        #     round(observed[num] * semiresult, 8)))
    print(np.diag(np.array(observed)))
    print(transition)
    print(result)
    result = np.diag(np.array(observed)).dot(transition).dot(result)
    # print('     {}'.format(result))
    print('')

print('sum: {}'.format(np.sum(result)))
