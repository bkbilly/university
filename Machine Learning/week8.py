import numpy as np

wq = np.array([
    [1, 1, 1],
    [0, 0, 3],
    [1, 0, 1],
    [1, 1, 0],
    [0, 0, 2]])

wk = np.array([
    [0, 0, 2],
    [0, 0, 3],
    [1, 0, 1],
    [1, 1, 0],
    [1, 1, 1]])
wv = np.array([
    [2, 3, 1],
    [0, 0, 2],
    [2, 0, 1],
    [0, 3, 0],
    [2, 0, 1]])

e = np.array([
    [0, 1, 0, 0, 3],
    [1, 0, 0, 1, 2],
    [0, 0, 2, 0, 1]
])

q = e.dot(wq)
k = e.dot(wk)
v = e.dot(wv)

print('Q:')
print(q)
print('K:')
print(k)
print('V:')
print(v)

# import ipdb
# ipdb.set_trace()
tmp1 = np.divide(q.dot(k.transpose()), np.sqrt(k.shape[1]))
print('TMP1:')
print(tmp1)

exp_sum = np.sum(np.exp(tmp1))
print('Exp sum:')
print(exp_sum)

print(np.divide(np.exp(tmp1), exp_sum))
