#!/usr/bin/env python3
# https://towardsdatascience.com/neural-machine-translation-with-python-c2f0a34f7dd
# https://github.com/susanli2016/NLP-with-Python

import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'  # or any {'0', '1', '2'}

import numpy as np
import pickle
import random
from tqdm import tqdm

from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.models import Model
from keras.models import load_model
from keras.layers import GRU, Input, Dense, TimeDistributed, Activation, RepeatVector, Bidirectional
from keras.layers.embeddings import Embedding
from keras.optimizers import Adam
from keras.losses import sparse_categorical_crossentropy
from keras.models import Sequential


# Tokenize (IMPLEMENTATION)
def tokenize(x):
    """
    Tokenize x
    :param x: List of sentences/strings to be tokenized
    :return: Tuple of (tokenized x data, tokenizer used to tokenize x)
    """
    # TODO: Implement
    x_tk = Tokenizer(char_level=False)
    x_tk.fit_on_texts(x)
    return x_tk.texts_to_sequences(x), x_tk


# Padding (IMPLEMENTATION)
def pad(x, length=None):
    """
    Pad x
    :param x: List of sequences.
    :param length: Length to pad the sequence to.  If None, use length of longest sequence in x.
    :return: Padded numpy array of sequences
    """
    # TODO: Implement
    if length is None:
        length = max([len(sentence) for sentence in x])
    return pad_sequences(x, maxlen=length, padding='post')


# Preprocess Pipeline
def preprocess(x, y, size=None):
    """
    Preprocess x and y
    :param x: Feature List of sentences
    :param y: Label List of sentences
    :return: Tuple of (Preprocessed x, Preprocessed y, x tokenizer, y tokenizer)
    """
    preprocess_x, x_tk = tokenize(x)
    preprocess_y, y_tk = tokenize(y)

    preprocess_x = pad(preprocess_x, length=size)
    preprocess_y = pad(preprocess_y, length=size)

    # Keras's sparse_categorical_crossentropy function requires the labels to be in 3 dimensions
    preprocess_y = preprocess_y.reshape(*preprocess_y.shape, 1)

    return preprocess_x, preprocess_y, x_tk, y_tk


# Ids Back to Text
def logits_to_text(logits, tokenizer):
    """
    Turn logits from a neural network into text using the tokenizer
    :param logits: Logits from a neural network
    :param tokenizer: Keras Tokenizer fit on the labels
    :return: String that represents the text of the logits
    """
    index_to_words = {id: word for word, id in tokenizer.word_index.items()}
    index_to_words[0] = '<PAD>'

    return ' '.join([index_to_words[prediction] for prediction in np.argmax(logits, 1)])


# Model 1: RNN (IMPLEMENTATION)
def simple_model(input_shape, output_sequence_length, english_vocab_size, french_vocab_size):
    """
    Build and train a basic RNN on x and y
    :param input_shape: Tuple of input shape
    :param output_sequence_length: Length of output sequence
    :param english_vocab_size: Number of unique English words in the dataset
    :param french_vocab_size: Number of unique French words in the dataset
    :return: Keras model built, but not trained
    """
    # TODO: Build the layers
    learning_rate = 1e-3
    input_seq = Input(input_shape[1:])
    rnn = GRU(64, return_sequences=True)(input_seq)
    logits = TimeDistributed(Dense(french_vocab_size))(rnn)
    model = Model(input_seq, Activation('softmax')(logits))
    model.compile(loss=sparse_categorical_crossentropy,
                  optimizer=Adam(learning_rate),
                  metrics=['accuracy'])

    return model


# Model 2: Embedding (IMPLEMENTATION)
def embed_model(input_shape, output_sequence_length, english_vocab_size, french_vocab_size):
    """
    Build and train a RNN model using word embedding on x and y
    :param input_shape: Tuple of input shape
    :param output_sequence_length: Length of output sequence
    :param english_vocab_size: Number of unique English words in the dataset
    :param french_vocab_size: Number of unique French words in the dataset
    :return: Keras model built, but not trained
    """
    # TODO: Implement
    learning_rate = 1e-3
    rnn = GRU(64, return_sequences=True, activation="tanh")

    embedding = Embedding(french_vocab_size, 64, input_length=input_shape[1])
    logits = TimeDistributed(Dense(french_vocab_size, activation="softmax"))

    model = Sequential()
    # em can only be used in first layer --> Keras Documentation
    model.add(embedding)
    model.add(rnn)
    model.add(logits)
    model.compile(loss=sparse_categorical_crossentropy,
                  optimizer=Adam(learning_rate),
                  metrics=['accuracy'])

    return model


# Model 3: Bidirectional RNNs (IMPLEMENTATION)
def bd_model(input_shape, output_sequence_length, english_vocab_size, french_vocab_size):
    """
    Build and train a bidirectional RNN model on x and y
    :param input_shape: Tuple of input shape
    :param output_sequence_length: Length of output sequence
    :param english_vocab_size: Number of unique English words in the dataset
    :param french_vocab_size: Number of unique French words in the dataset
    :return: Keras model built, but not trained
    """
    # TODO: Implement
    learning_rate = 1e-3
    model = Sequential()
    model.add(Bidirectional(GRU(128, return_sequences=True, dropout=0.1),
                            input_shape=input_shape[1:]))
    model.add(TimeDistributed(Dense(french_vocab_size, activation='softmax')))
    model.compile(loss=sparse_categorical_crossentropy,
                  optimizer=Adam(learning_rate),
                  metrics=['accuracy'])
    return model


# Model 4: Encoder-Decoder (OPTIONAL)
def encdec_model(input_shape, output_sequence_length, english_vocab_size, french_vocab_size):
    """
    Build and train an encoder-decoder model on x and y
    :param input_shape: Tuple of input shape
    :param output_sequence_length: Length of output sequence
    :param english_vocab_size: Number of unique English words in the dataset
    :param french_vocab_size: Number of unique French words in the dataset
    :return: Keras model built, but not trained
    """
    # OPTIONAL: Implement
    learning_rate = 1e-3
    model = Sequential()
    model.add(GRU(128, input_shape=input_shape[1:], return_sequences=False))
    model.add(RepeatVector(output_sequence_length))
    model.add(GRU(128, return_sequences=True))
    model.add(TimeDistributed(Dense(french_vocab_size, activation='softmax')))

    model.compile(loss=sparse_categorical_crossentropy,
                  optimizer=Adam(learning_rate),
                  metrics=['accuracy'])
    return model


# Model 5: Custom (IMPLEMENTATION)
def model_final(input_shape, output_sequence_length, english_vocab_size, french_vocab_size):
    """
    Build and train a model that incorporates embedding, encoder-decoder, and bidirectional RNN on x and y
    :param input_shape: Tuple of input shape
    :param output_sequence_length: Length of output sequence
    :param english_vocab_size: Number of unique English words in the dataset
    :param french_vocab_size: Number of unique French words in the dataset
    :return: Keras model built, but not trained
    """
    # TODO: Implement
    model = Sequential()
    model.add(Embedding(input_dim=english_vocab_size,
                        output_dim=128, input_length=input_shape[1]))
    model.add(Bidirectional(GRU(256, return_sequences=False)))
    model.add(RepeatVector(output_sequence_length))
    model.add(Bidirectional(GRU(256, return_sequences=True)))
    model.add(TimeDistributed(Dense(french_vocab_size, activation='softmax')))
    learning_rate = 0.005

    model.compile(loss=sparse_categorical_crossentropy,
                  optimizer=Adam(learning_rate),
                  metrics=['accuracy'])

    return model


# Prediction (IMPLEMENTATION)
def run_eval(x, y, x_tk, y_tk, x_val, y_val, model_name, epochs=17, batch_size=10):
    """
    Gets predictions using the final model
    :param x: Preprocessed English data
    :param y: Preprocessed German data
    :param x_tk: English tokenizer
    :param y_tk: German tokenizer
    :param x_val: Preprocessed English validation data 
    :param y_val: Preprocessed German validation data
    """

    model_path = 'models/translate_model_{}.h5'.format(model_name)
    history_path = 'models/translate_model_{}.hist'.format(model_name)
    if not os.path.exists(model_path):
        model = model_final(x.shape,
                            y.shape[1],
                            len(x_tk.word_index) + 1,
                            len(y_tk.word_index) + 1)
        # print(x.shape)
        # print(y.shape)
        # print(x_val.shape)
        # print(y_val.shape)
        # history = model.fit(pad(x), y, epochs=17, validation_split=0.2)
        history = model.fit(
            pad(x),
            y,
            epochs=epochs,
            validation_data=(pad(x_val), y_val),
            batch_size=batch_size)
        model.save(model_path)
        with open(history_path, 'wb') as file_pi:
            pickle.dump(history.history, file_pi)
    model = load_model(model_path)
    history = pickle.load(open(history_path, 'rb'))


    #### Make prediction
    eng_sentences = []
    ger_sentences = []
    input_file = 'data/german/ger-test.txt'
    with open(input_file) as f:
        for line in f:
            ger_sentence, eng_sentence = line.split('\t')
            eng_sentences.append(eng_sentence.replace('\n', ''))
            ger_sentences.append(ger_sentence.replace('\n', ''))

    eng_preproc, ger_preproc, eng_tkn, ger_tkn = preprocess(eng_sentences, ger_sentences)
    y_id_to_word = {value: key for key, value in y_tk.word_index.items()}
    y_id_to_word[0] = '<PAD>'
    # print(eng_tkn.word_index)
    # print(eng_preproc.shape)
    # print(len(eng_sentences))
    ger_predictions = []
    # for num, sentence in enumerate(eng_preproc):
    for num, sentence in enumerate(tqdm(eng_preproc)):
        sentence = pad_sequences([sentence], maxlen=x.shape[-1], padding='post')
        sentences = np.array([sentence[0], x[0]])
        predictions = model.predict(sentences, len(sentences))

        # print(logits_to_text(embeded_model.predict(tmp_x[:1])[0], french_tokenizer))
        # print('predictions:', len(predictions))
        try:
            prediction = ' '.join([y_id_to_word[np.argmax(x)]
                                  for x in predictions[0]]).replace('<PAD>', '').replace('\n', '').strip()
            ger_predictions.append(prediction)

            reference = [ger_sentences[num].split(' ')]
            candidate = prediction.split(' ')
            print('from: {}\nto: {}\npred: {}\n'.format(
                eng_sentences[num],
                ger_sentences[num],
                prediction,
            ))
        except Exception as e:
            ger_predictions.append('')
            print(eng_sentences[num], e)

    with open("ger_predictions.txt", "w") as outfile:
        outfile.write("\n".join(ger_predictions))
    with open("eng_sentences.txt", "w") as outfile:
        outfile.write("\n".join(eng_sentences))
    with open("ger_sentences.txt", "w") as outfile:
        outfile.write("\n".join(ger_sentences))



    return model, history

def read_data(input_file, input_file_2=None, input2_type='simple', trim_data=None):
    """
    Reads a file with German sentences and the English translation.
    If only one input_file is defined, then it will should ba tab seperated.
    If two input_files are defined, then the 1st is English and the other German.
    """
    val_eng_sentences = []
    val_ger_sentences = []
    tmp_eng_sentences = []
    tmp_ger_sentences = []
    eng_sentences = []
    ger_sentences = []

    if input2_type == 'simple':
        with open(input_file) as f:
            for line in f:
                ger_sentence, eng_sentence = line.split('\t')
                tmp_eng_sentences.append(eng_sentence)
                tmp_ger_sentences.append(ger_sentence)
    elif input2_type == '2ndlanguage':
        with open(input_file) as f:
            tmp_eng_sentences = f.readlines()
        with open(input_file_2) as f:
            tmp_ger_sentences = f.readlines()
    elif input2_type == 'validation':
        with open(input_file) as f:
            for line in f:
                ger_sentence, eng_sentence = line.split('\t')
                tmp_eng_sentences.append(eng_sentence)
                tmp_ger_sentences.append(ger_sentence)
        with open(input_file_2) as f:
            for line in f:
                ger_sentence, eng_sentence = line.split('\t')
                val_eng_sentences.append(eng_sentence)
                val_ger_sentences.append(ger_sentence)
    else:
        return 0

    # check input lengths to be the same
    if len(tmp_ger_sentences) != len(tmp_eng_sentences):
        return 0

    # Trim results
    if trim_data is not None:
        tmp_eng_sentences = tmp_eng_sentences[:trim_data]
        tmp_ger_sentences = tmp_ger_sentences[:trim_data]

    sentences_size = len(tmp_ger_sentences)

    # Get the validation data if not provided
    random.seed(1)
    ratio = 0.2
    if len(val_eng_sentences) == 0:
        for num in range(sentences_size):
            if random.random() < ratio:
                val_eng_sentences.append(tmp_eng_sentences[num])
                val_ger_sentences.append(tmp_ger_sentences[num])
            else:
                eng_sentences.append(tmp_eng_sentences[num])
                ger_sentences.append(tmp_ger_sentences[num])
    else:
        eng_sentences = tmp_eng_sentences
        ger_sentences = tmp_ger_sentences


    # Find the maximum word length
    max_length = 0
    for sentence in [*eng_sentences, *ger_sentences, *val_eng_sentences, *val_ger_sentences]:
        word_counter = len(sentence.split(' '))
        if max_length < word_counter:
            max_length = word_counter


    return eng_sentences, ger_sentences, val_eng_sentences, val_ger_sentences, max_length


# eng_sentences, ger_sentences, max_length = read_data('data/german/ger-train.txt')
# val_eng_sentences, val_ger_sentences, val_max_length = read_data('data/german/ger-validation.txt')
# real_max = max(max_length, val_max_length)
# eng_sentences, ger_sentences, val_eng_sentences, val_ger_sentences, real_max = read_data(
#     'data/german/ger-train.txt',
#     'data/german/ger-validation.txt',
#     'validation'
#     )
eng_sentences, ger_sentences, val_eng_sentences, val_ger_sentences, real_max = read_data(
    'data/german_full/train.en',
    'data/german_full/train.de',
    '2ndlanguage',
    trim_data=170000
    )
print(len(eng_sentences))
print(len(ger_sentences))
print(len(val_eng_sentences))
print(len(val_ger_sentences))
print(real_max)

eng_preproc, ger_preproc, eng_tkn, ger_tkn = preprocess(eng_sentences, ger_sentences, real_max)
val_eng_preproc, val_ger_preproc, val_eng_tkn, val_ger_tkn = preprocess(val_eng_sentences, val_ger_sentences, real_max)

print("  Max sentence length:", real_max)
print("  English train size:", len(eng_tkn.word_index))
print("  German train size:", len(ger_tkn.word_index))
print("  English validation size:", len(val_eng_tkn.word_index))
print("  German validation size:", len(val_ger_tkn.word_index))


run_eval(eng_preproc, ger_preproc, eng_tkn,
         ger_tkn, val_eng_preproc, val_ger_preproc,
         'trimed', 5)
