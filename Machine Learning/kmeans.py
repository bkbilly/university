import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import random as rd

df = pd.read_csv('kmeans.csv', names=['x', 'y']).to_numpy()


df = df

m, n = df.shape
n_iter = 100
K = 5

Centroids = np.array([]).reshape(n, 0)
for i in range(K):
    rand = rd.randint(0, m - 1)
    Centroids = np.c_[Centroids, df[rand]]


Output = {}
EuclidianDistance = np.array([]).reshape(m, 0)
for k in range(K):
    tempDist = np.sum((df - Centroids[:, k])**2, axis=1)
    EuclidianDistance = np.c_[EuclidianDistance, tempDist]
C = np.argmin(EuclidianDistance, axis=1) + 1
print(C)


Y = {}
for k in range(K):
    Y[k + 1] = np.array([]).reshape(2, 0)
for i in range(m):
    Y[C[i]] = np.c_[Y[C[i]], df[i]]
for k in range(K):
    Y[k + 1] = Y[k + 1].T
for k in range(K):
    Centroids[:, k] = np.mean(Y[k + 1], axis=0)
print(Y)

for i in range(n_iter):
     # step 2.a
    EuclidianDistance = np.array([]).reshape(m, 0)
    for k in range(K):
        tempDist = np.sum((df - Centroids[:, k])**2, axis=1)
        EuclidianDistance = np.c_[EuclidianDistance, tempDist]
    C = np.argmin(EuclidianDistance, axis=1) + 1
    # step 2.b
    Y = {}
    for k in range(K):
        Y[k + 1] = np.array([]).reshape(2, 0)
    for i in range(m):
        Y[C[i]] = np.c_[Y[C[i]], df[i]]

    for k in range(K):
        Y[k + 1] = Y[k + 1].T

    for k in range(K):
        Centroids[:, k] = np.mean(Y[k + 1], axis=0)
    Output = Y


print(Output)

plt.scatter(df[:, 0], df[:, 1])
plt.scatter(Centroids[0, :], Centroids[1, :])
plt.show()
