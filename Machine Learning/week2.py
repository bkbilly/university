#!/usr/bin/env python3

import math

yt = 1
n = 0.1

x0 = 1
x1 = 1
x2 = 0

w45 = 4
w35 = 2
w05 = -3.93
w03 = 1
w04 = -6
w13 = 3
w14 = 6
w23 = 4
w24 = 5

for i in range(1, 4):

    a3 = x0 * w03 + x1 * w13 + x2 * w23
    a4 = x0 * w04 + x1 * w14 + x2 * w24
    y3 = 1 / (1 + math.pow(math.e, (-a3)))
    y4 = 1 / (1 + math.pow(math.e, (-a4)))
    a5 = x0 * w05 + y3 * w35 + y4 * w45
    y5 = 1 / (1 + math.pow(math.e, (-a5)))

    error = yt - y5

    # print a3, a4, y3, y4, a5, y5, error

    d5 = (yt - y5) * y5 * (1 - y5)
    d4 = y4 * (1 - y4) * d5 * w45
    d3 = y3 * (1 - y3) * d5 * w35

    # print d5, d4, d3

    dw45 = n * d5 * y4
    dw35 = n * d5 * y3
    dw05 = n * d5 * x0
    dw03 = n * d3 * x0
    dw04 = n * d4 * x0
    dw13 = n * d3 * x1
    dw14 = n * d4 * x1
    dw23 = n * d3 * x2
    dw24 = n * d4 * x2

    # print dw45, dw35, dw05, dw03, dw04, dw13, dw14, dw23, dw24
    w45 += dw45
    w35 += dw35
    w05 += dw05
    w03 += dw03
    w04 += dw04
    w13 += dw13
    w14 += dw14
    w23 += dw23
    w24 += dw24

    print('back {}:'.format(i))
    print('  w03: {}'.format(w03))
    print('  w04: {}'.format(w04))
    print('  w05: {}'.format(w05))
    print('  w13: {}'.format(w13))
    print('  w14: {}'.format(w14))
    print('  w23: {}'.format(w23))
    print('  w24: {}'.format(w24))
    print('  w35: {}'.format(w35))
    print('  w45: {}'.format(w45))
    print('  Error: {}'.format(error))
