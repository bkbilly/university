from pathlib import Path
import matplotlib.pyplot as plt
import numpy as np
from sklearn import svm, metrics, datasets
from sklearn.utils import Bunch
from sklearn.model_selection import GridSearchCV, train_test_split

from skimage.io import imread
from skimage.transform import resize


def trim_images(from_folder, to_folder, ratio):
    import os
    import shutil
    import random
    # Random catdogs for Training
    random.seed(1)
    no_cats, no_dogs = 0, 0
    try:
        shutil.rmtree(to_folder)
    except:
        pass
    os.mkdir(to_folder)
    os.mkdir(to_folder + 'cats/')
    os.mkdir(to_folder + 'dogs/')

    for file in os.listdir(from_folder):
        src = from_folder + file
        if random.random() < ratio:
            if file.startswith('cat'):
                dst = to_folder + 'cats/' + file
                shutil.copyfile(src, dst)
                no_cats += 1
            elif file.startswith('dog'):
                dst = to_folder + 'dogs/' + file
                shutil.copyfile(src, dst)
                no_dogs += 1
    return no_cats, no_dogs


def load_image_files(container_path, dimension=(64, 64)):
    """
    Load image files with categories as subfolder names 
    which performs like scikit-learn sample dataset

    Parameters
    ----------
    container_path : string or unicode
        Path to the main folder holding one subfolder per category
    dimension : tuple
        size to which image are adjusted to

    Returns
    -------
    Bunch
    """
    image_dir = Path(container_path)
    folders = [directory for directory in image_dir.iterdir()
               if directory.is_dir()]
    categories = [fo.name for fo in folders]

    descr = "A image classification dataset"
    images = []
    flat_data = []
    target = []
    for i, direc in enumerate(folders):
        for file in direc.iterdir():
            img = imread(file)
            img_resized = resize(
                img, dimension, anti_aliasing=True, mode='reflect')
            flat_data.append(img_resized.flatten())
            images.append(img_resized)
            target.append(i)
    flat_data = np.array(flat_data)
    target = np.array(target)
    images = np.array(images)

    return Bunch(data=flat_data,
                 target=target,
                 target_names=categories,
                 images=images,
                 DESCR=descr)

trim_images('cnn/machinelearning/test/', 'cnn/machinelearning/testnew/', 1)

image_dataset = load_image_files("cnn/machinelearning/testnew")

X_train, X_test, y_train, y_test = train_test_split(
    image_dataset.data, image_dataset.target, test_size=0.3, random_state=109)
print(X_train.shape)

param_grid = [
    {'C': [1, 10, 100, 1000], 'kernel': ['linear']},
    {'C': [1, 10, 100, 1000], 'gamma': [0.001, 0.0001], 'kernel': ['rbf']},
]
svc = svm.SVC()
clf = GridSearchCV(svc, param_grid, verbose=1)
grid_result = clf.fit(X_train, y_train)
import ipdb
ipdb.set_trace()

y_pred = clf.predict(X_test)

print("Classification report for - \n{}:\n{}\n".format(
    clf, metrics.classification_report(y_test, y_pred)))
