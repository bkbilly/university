import sklearn
import pandas as pd

import matplotlib.pyplot as plt
import numpy as np
from sklearn import linear_model
from sklearn.metrics import mean_squared_error, r2_score

data = pd.read_csv('prostate_dataset.txt', delimiter="\t")
data.drop(["col"], axis=1, inplace=True)
df = pd.DataFrame(data=data)

train_set = data[data["train"] == "T"]
train_labels = train_set["lpsa"]
train_set.drop(["train", "lpsa"], axis=1, inplace=True)

test_set = data[data["train"] == "F"]
test_labels = test_set["lpsa"]
test_set.drop(["train", "lpsa"], axis=1, inplace=True)

# regr = linear_model.LinearRegression()
# regr = linear_model.Lasso(alpha=0.1)
regr = linear_model.Ridge(alpha=0.1)


regr.fit(train_set, train_labels)

y_pred = regr.predict(test_set)

# The coefficients
print('Coefficients: \n', regr.coef_)
# The mean squared error
print('Mean squared error: %.2f'
      % mean_squared_error(test_labels, y_pred))
# The coefficient of determination: 1 is perfect prediction
print('Coefficient of determination: %.2f'
      % r2_score(test_labels, y_pred))

# Plot outputs
plt.scatter(test_set["age"], test_labels, color='blue', linewidth=3)

# plt.plot(test_set["age"], y_pred, color='blue', linewidth=3)

plt.xticks(())
plt.yticks(())

plt.show()
