#!/bin/bash

apt install -y python3-pip htop lm-sensors
pip3 install --upgrade pip
apt remove python3-wrapt 
apt remove python3-scipy

if [ -z $(which subl) ]; then
    wget -qO - https://download.sublimetext.com/sublimehq-pub.gpg | apt-key add -
    apt install -y apt-transport-https
    echo "deb https://download.sublimetext.com/ apt/stable/" | tee /etc/apt/sources.list.d/sublime-text.list
    apt update
    apt install -y sublime-text
fi

if [ -z $(which nvtop) ]; then
    apt install -y cmake libncurses5-dev libncursesw5-dev git
    git clone https://github.com/Syllo/nvtop.git
    mkdir -p nvtop/build
    cd nvtop/build
    cmake ..
    cmake .. -DNVML_RETRIEVE_HEADER_ONLINE=True
    make
    make install
    cd ~
fi

wget https://developer.download.nvidia.com/compute/cuda/repos/ubuntu1804/x86_64/cuda-repo-ubuntu1804_10.1.243-1_amd64.deb
apt-key adv --fetch-keys https://developer.download.nvidia.com/compute/cuda/repos/ubuntu1804/x86_64/7fa2af80.pub
dpkg -i cuda-repo-ubuntu1804_10.1.243-1_amd64.deb
wget http://developer.download.nvidia.com/compute/machine-learning/repos/ubuntu1804/x86_64/nvidia-machine-learning-repo-ubuntu1804_1.0.0-1_amd64.deb
apt install -y ./nvidia-machine-learning-repo-ubuntu1804_1.0.0-1_amd64.deb
apt update
apt install -y --no-install-recommends nvidia-driver-430

apt install -y --no-install-recommends cuda-10-1 libcudnn7=7.6.4.38-1+cuda10.1 libcudnn7-dev=7.6.4.38-1+cuda10.1
apt install -y --no-install-recommends libnvinfer6=6.0.1-1+cuda10.1 libnvinfer-dev=6.0.1-1+cuda10.1 libnvinfer-plugin6=6.0.1-1+cuda10.1

pip3 install tensorflow-gpu
pip3 install matplotlib opencv-python numpy

