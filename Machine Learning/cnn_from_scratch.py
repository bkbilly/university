#!/usr/bin/env python3

from PIL import Image
import numpy as np


def initializeFilter(size, scale=1.0):
    '''
    Initialize filter using a normal distribution with and a 
    standard deviation inversely proportional the square root of the number of units
    '''
    # print(size)
    stddev = scale / np.sqrt(np.prod(size))
    return np.random.normal(loc=0, scale=stddev, size=size)


def initializeWeight(size):
    '''
    Initialize weights with a random normal distribution
    '''
    return np.random.standard_normal(size=size) * 0.01


def convolution(image, filt, bias, s=1):
    '''
    Confolves `filt` over `image` using stride `s`
    '''
    (n_f, n_c_f, f, _) = filt.shape  # filter dimensions
    n_c, in_dim, _ = image.shape  # image dimensions
    print(n_c, n_c_f)

    out_dim = int((in_dim - f) / s) + 1  # calculate output dimensions

    # ensure that the filter dimensions match the dimensions of the input image
    assert n_c == n_c_f, "Dimensions of filter must match dimensions of input image"

    # create the matrix to hold the values of the convolution operation
    out = np.zeros((n_f, out_dim, out_dim))

    # convolve each filter over the image
    for curr_f in range(n_f):
        curr_y = out_y = 0
        # move filter vertically across the image
        while curr_y + f <= in_dim:
            curr_x = out_x = 0
            # move filter horizontally across the image
            while curr_x + f <= in_dim:
                # perform the convolution operation and add the bias
                out[curr_f, out_y, out_x] = np.sum(
                    filt[curr_f] * image[
                        :,
                        curr_y:curr_y + f,
                        curr_x:curr_x + f]
                ) + bias[curr_f]
                curr_x += s
                out_x += 1
            curr_y += s
            out_y += 1

    return out


def maxpool(image, f=2, s=2):
    '''
    Downsample input `image` using a kernel size of `f` and a stride of `s`
    '''
    n_c, h_prev, w_prev = image.shape

    # calculate output dimensions after the maxpooling operation.
    h = int((h_prev - f) / s) + 1
    w = int((w_prev - f) / s) + 1

    # create a matrix to hold the values of the maxpooling operation.
    downsampled = np.zeros((n_c, h, w))

    # slide the window over every part of the image using stride s. Take the maximum value at each step.
    for i in range(n_c):
        curr_y = out_y = 0
        # slide the max pooling window vertically across the image
        while curr_y + f <= h_prev:
            curr_x = out_x = 0
            # slide the max pooling window horizontally across the image
            while curr_x + f <= w_prev:
                # choose the maximum value within the window at each step and store it to the output matrix
                downsampled[i, out_y, out_x] = np.max(
                    image[
                        i,
                        curr_y:curr_y + f,
                        curr_x:curr_x + f])
                curr_x += s
                out_x += 1
            curr_y += s
            out_y += 1
    return downsampled


image = np.array(Image.open(
    '/home/bkbilly/Downloads/task1-train/train/dog.0.jpg'))

# num_epochs = 2
num_filt1 = 1
img_depth = 375
f = 1
f1 = initializeFilter((num_filt1, img_depth, f, f))
print(f1.shape)
print(image.shape)
# w3 = initializeWeight((128, 800))
b1 = np.zeros((f1.shape[0], 1))


convolution(image, f1, b1)
