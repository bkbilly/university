import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

df = pd.DataFrame({
    'x': [1, 0.5, -2, -3, -1.5, -2.5, -1.5, 2],
    'y': [1, 0, -2, -5, -3, -3, -2.5, 2.5]
})



np.random.seed(200)
k = 2
# centroids[i] = [x, y]
centroids = {
    i + 1: [np.random.randint(df.min()['x'], df.max()['x']), np.random.randint(df.min()['y'], df.max()['y'])]
    for i in range(k)
}
print(centroids)
fig = plt.figure(figsize=(5, 5))
plt.scatter(df['x'], df['y'], color='k')
colmap = {1: 'r', 2: 'g'}
for i in centroids.keys():
    plt.scatter(*centroids[i], color=colmap[i])
plt.xlim(df.min()['x'], df.max()['x'])
plt.ylim(df.min()['y'], df.max()['y'])
plt.show()
