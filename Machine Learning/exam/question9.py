import csv
import pandas as pd


cases = list(csv.DictReader(open('question9.csv', newline=''), delimiter=','))
print(cases, '\n')

category_cases = {}
for case in cases:
    for key, value in case.items():
        category_cases.setdefault(key, [])
        if value not in category_cases[key]:
            category_cases[key].append(value)
print(category_cases, '\n')


categories = ['weather', 'season']
# categories = ['weather']
# categories = ['season']
# categories = ['toc']
categories = ['weather', 'season', 'toc', 'day']
# categories = ['season']

d = category_cases
product_cat = [d[cat] for cat in categories]
df = pd.DataFrame(
    index=pd.MultiIndex.from_product(product_cat),
    columns=d['lateness'], data=0
)

for case in cases:
    attributes = []
    for category in categories:
        attributes.append(case[category])
    df.loc[tuple(attributes), case['lateness']] += 1

print(df)
print('')
print(df.loc['windy', 'autumn', 'rotrail', 'day'])
print(df.loc['calm', 'summer', 'virgo', 'day'])
print(df.loc['calm', 'spring', 'gnaf', 'end'])
