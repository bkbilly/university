import numpy as np
import matplotlib.pyplot as plt
from skimage.morphology import max_tree
from skimage.morphology import area_opening
from skimage.morphology import area_closing
# from skimage.morphology import intensity_opening
from skimage.morphology import diameter_opening
from skimage.morphology import diameter_closing
# from skimage.morphology import attribute_opening
from skimage import io
from skimage.transform import resize

# pip install scikit-image


image_leaf = io.imread(
    'org data/ISIC_0000042.jpg',
    as_gray=True)
image_leaf = resize(image_leaf, (347, 543), anti_aliasing=True)

image_leaf = (image_leaf * 255).astype(int)
image_leaf = (255 - image_leaf)
P_leaf, S_leaf = max_tree(image_leaf)

fig, axs = plt.subplots(2, 1, sharey=False, figsize=(15, 10))

axs[0].imshow(image_leaf, cmap='gray', vmin=0, vmax=255)
axs[0].set_title('HOLY LEAF')

leaf_f1 = area_closing(image_leaf, 40000, connectivity=1,
                       parent=P_leaf, tree_traverser=S_leaf)
axs[1].imshow(leaf_f1, cmap='gray', vmin=0, vmax=255)
axs[1].set_title('AREA OPEN 4')

plt.show()
