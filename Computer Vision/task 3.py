#!/usr/bin/env python3
import matplotlib.pyplot as plt
from pykalman import KalmanFilter
import numpy as np


def evaluate(gt, km):
    ''' Compares the ground truth results with the output of kelman filter '''
    mean = np.mean(km - gt)
    std = np.std(km - gt)
    rmse = np.sqrt(np.mean((km - gt)**2))
    print('mean: {}'.format(round(mean, 3)))
    print('std: {}'.format(round(std, 3)))
    print('rmse: {}'.format(round(rmse, 3)))


def showplot(noisy, target, smoothed):
    ''' Plot the original coordinates of:
        [a, b] wihch is the noise
        [x, y] which is the ground truth
        and the output of the kelman filter '''
    plt.figure(1)
    plt.plot(noisy[:, 0], noisy[:, 1], linewidth=1,
             linestyle='--', color='black', label='noisy')
    plt.plot(target[:, 0], target[:, 1], linewidth=3,
             linestyle='-', color='green', label='ground truth')
    plt.plot(smoothed[:, 0], smoothed[:, 1], linewidth=2,
             linestyle='-', color='blue', label='kalman filter')
    plt.legend()
    plt.show()


# Read the CSV files into arrays
x = list(np.genfromtxt('x.csv', dtype=float, delimiter=','))
y = list(np.genfromtxt('y.csv', dtype=float, delimiter=','))
a = list(np.genfromtxt('a.csv', dtype=float, delimiter=','))
b = list(np.genfromtxt('b.csv', dtype=float, delimiter=','))

target = np.asarray(list(zip(x, y)))
noisy = np.asarray(list(zip(a, b)))


# Initiate Kelman Filter with its matrices

initial_state_mean = [
    noisy[0, 0],
    0,
    noisy[0, 1],
    0]

transition_matrix = [
    [1, 1, 0, 0],
    [0, 1, 0, 0],
    [0, 0, 1, 1],
    [0, 0, 0, 1]]

observation_matrix = [
    [1, 0, 0, 0],
    [0, 0, 1, 0]]

transition_covariance = [
    [0.1, 1, 0, 0],
    [0, 0.2, 0, 0],
    [0, 0, 0.1, 1],
    [0, 0, 0, 0.2]]
observation_covariance = [
    [0.3, 0],
    [0, 0.3]]

kf1 = KalmanFilter(
    transition_matrices=transition_matrix,
    observation_matrices=observation_matrix,
    initial_state_mean=initial_state_mean,
    transition_covariance=transition_covariance,
    observation_covariance=observation_covariance
)

# Apply Kelman Filter
kf1 = kf1.em(noisy, n_iter=10)
(smoothed_state_means, smoothed_state_covariances) = kf1.smooth(noisy)
smoothed = np.stack(
    (smoothed_state_means[:, 0], smoothed_state_means[:, 2]), axis=-1)

print('-- Noise vs Ground Truth--')
evaluate(noisy, smoothed)
print('-- Prediction vs Ground Truth--')
evaluate(target, smoothed)

showplot(noisy, target, smoothed)
