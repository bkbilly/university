#!/usr/bin/env python3
import numpy as np
import cv2
from PIL import Image
import matplotlib.pyplot as plt
import os


from_folder = 'org data/'


def cnt_from_image(file, showimage=True):

    src = from_folder + file

    # Read the image and perfrom an OTSU threshold
    img = cv2.imread(src)
    kernel = np.ones((15, 15), np.uint8)

    # Perform closing to remove hair and blur the image
    closing = cv2.morphologyEx(img, cv2.MORPH_CLOSE, kernel, iterations=2)
    blur = cv2.blur(closing, (15, 15))

    # Binarize the image
    gray = cv2.cvtColor(blur, cv2.COLOR_BGR2GRAY)
    _, thresh = cv2.threshold(
        gray, 0, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)

    # Search for contours
    contours, hierarchy = cv2.findContours(
        thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
    filtered_contours = []

    # Remove the corner contours
    img_width, img_height = gray.shape
    for cnt in contours:
        cnt_1 = np.count_nonzero(cnt == 0)
        cnt_2 = np.count_nonzero(cnt == img_width)
        cnt_3 = np.count_nonzero(cnt == img_height)
        if cnt_1 <= 30 and cnt_2 <= 30 and cnt_3 <= 30:
            filtered_contours.append(cnt)
    if len(filtered_contours) > 0:
        contours = filtered_contours

    #  Get the biggest contour
    cnt = max(contours, key=cv2.contourArea)

    # Create a new mask for the result image
    h, w = img.shape[:2]
    final_mask = np.zeros((h, w), np.uint8)

    # Draw the contour on the new mask and perform the bitwise operation
    cv2.drawContours(final_mask, [cnt], -1, 255, -1)
    final = cv2.bitwise_and(img, img, mask=final_mask)

    # Read the Ground Truth images
    gt_src = src.replace('org data', 'GT').replace('.jpg', '_Segmentation.png')
    gt_img = cv2.imread(gt_src, cv2.IMREAD_GRAYSCALE)
    final_mask = final_mask / 255
    gt_img = gt_img / 255

    # Calculate the Dice Similarity Score
    dice = 2 * np.sum(final_mask[gt_img == 1]) / \
        (np.sum(final_mask) + np.sum(gt_img))

    # Show all images in a plot
    if showimage:  # or dice <= 0.5:
        fig, axs = plt.subplots(2, 2, sharey=False, figsize=(15, 10))
        fig.suptitle('{}\n dice={}%'.format(file, round(100 * dice, 2)), fontsize=16)
        axs[0][0].set_title('Original')
        axs[0][0].imshow(img)
        axs[0][1].set_title('Final Image')
        axs[0][1].imshow(final)
        axs[1][0].set_title('Final Mask')
        axs[1][0].imshow(final_mask)
        axs[1][1].set_title('Ground Trouth')
        axs[1][1].imshow(gt_img)
        plt.show()

    return dice


img_numbers = []
img_dice = []
for file in os.listdir(from_folder):
    dice = cnt_from_image(file, showimage=False)
    print(file, dice)
    img_numbers.append(file.replace('.jpg', '').replace(
        'ISIC_00000', '').replace('ISIC_0000', ''))
    img_dice.append(dice)

ds_mean = round(100 * sum(img_dice) / len(img_dice), 2)
y_pos = np.arange(len(img_numbers))
plt.bar(y_pos, img_dice, align='center', alpha=0.5)
plt.xticks(y_pos, img_numbers, fontsize=7, rotation=30)
plt.ylabel('Dice Similarity Score')
plt.xlabel('Image Number')
plt.title('Skin lesion segmentation\nmean={}%'.format(ds_mean))
plt.show()


cnt_from_image('ISIC_0000019.jpg', showimage=True)
cnt_from_image('ISIC_0000095.jpg', showimage=True)
cnt_from_image('ISIC_0000214.jpg', showimage=True)
