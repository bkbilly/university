import cv2
import numpy as np
from matplotlib import pyplot as plt
from PIL import Image, ImageDraw
from skimage.feature import greycomatrix, greycoprops


def create_spectrum(img, plot=True):
    # Convert the image to a spectrum using FFT
    f = np.fft.fft2(img)
    fshift = np.fft.fftshift(f)
    magnitude_spectrum = 20 * np.log(np.abs(fshift))

    if plot:
        plt.subplot(121), plt.imshow(img, cmap='gray')
        plt.title('Input Image'), plt.xticks([]), plt.yticks([])
        plt.subplot(122), plt.imshow(magnitude_spectrum, cmap='gray')
        plt.title('Magnitude Spectrum'), plt.xticks([]), plt.yticks([])
        plt.show()
    return magnitude_spectrum


def get_circle_sum(input_img, radius, plot=True):
    # Create new empty image with same size as the original
    # and draw a circle with specific radius to get the mask
    image_size = (input_img.shape[1], input_img.shape[0])
    pil_mask = Image.new('L', size=image_size, color=255)
    x = pil_mask.size[0] / 2
    y = pil_mask.size[1] / 2

    draw = ImageDraw.Draw(pil_mask)
    draw.ellipse((x - radius, y - radius, x + radius, y + radius), fill=0)

    # Mask the original image to get only what's inside the circle
    inpt_img = Image.fromarray(input_img)
    empty_img = Image.new('L', size=image_size, color=0)
    cropped_pil_img = Image.composite(empty_img, inpt_img, pil_mask)
    cropped_img = np.array(cropped_pil_img)

    if plot:
        plt.subplot(111), plt.imshow(cropped_img, cmap='gray')
        plt.show()

    return np.sum(cropped_img)


def get_line_sum(input_img, degrees, width=100, plot=True):
    # Create new empty image with same size as the original
    # and draw a line with specific width to get the mask

    image_size = (input_img.shape[1], input_img.shape[0])
    pil_mask = Image.new('L', size=image_size, color=255)
    x = pil_mask.size[0]
    y = pil_mask.size[1]

    draw = ImageDraw.Draw(pil_mask)
    if degrees == 0:
        draw.line((0, y / 2, x, y / 2), width=width, fill=0)
    elif degrees == 45:
        draw.line((0, y, x, 0), width=width, fill=0)
    elif degrees == 90:
        draw.line((x / 2, 0, x / 2, y), width=width, fill=0)
    elif degrees == 135:
        draw.line((0, 0, x, y), width=width, fill=0)
    else:
        print('wrong coordinates, choose between these:', [0, 45, 90, 135])
        return 0

    # Mask the original image to get only what's inside the circle
    inpt_img = Image.fromarray(input_img)
    empty_img = Image.new('L', size=image_size, color=0)
    cropped_pil_img = Image.composite(empty_img, inpt_img, pil_mask)
    cropped_img = np.array(cropped_pil_img)

    if plot:
        plt.subplot(111), plt.imshow(cropped_img, cmap='gray')
        plt.show()

    return np.sum(cropped_img)


def read_image(image_path, coordinates=None, size=None, depth=None, plot=False):
    ''' Reads, crops and changes bit-depth if required of the provided image. '''
    in_img = cv2.imread(image_path, cv2.IMREAD_GRAYSCALE)
    if coordinates is None or coordinates == ():
        out_img = in_img
    else:
        out_img = in_img[coordinates[1]:coordinates[1] + size[1], coordinates[0]:coordinates[0] + size[0]]

    if plot:
        plt.subplot(121), plt.imshow(in_img, cmap='gray')
        plt.subplot(122), plt.imshow(out_img, cmap='gray')
        plt.show()

    if depth is not None:
        division = int(256 / (2 ** depth))
        out_img = np.divide(out_img, division)
        out_img = np.round(out_img, decimals=0)
        # print(out_img)
        out_img = out_img.astype(np.uint8)
    return in_img, out_img


def get_histogram(img, depth=8, plot=False):
    ''' Create a histogram based on the bit-depth that is requested '''

    size_pix = int(2 ** depth)
    histogram = img.ravel(), size_pix, [0, size_pix]
    if plot:
        plt.hist(histogram)
        plt.show()
    return histogram


def get_cooccurence(img, angles, plot=False):
    ''' gets the coocurence matrix '''
    result = greycomatrix(img, distances=[1], angles=angles)
    comatrix_img = result[:, :, 0, 0]
    if plot:
        plt.subplot(111), plt.imshow(comatrix_img)
        plt.show()

    return comatrix_img, result


def get_textures(comatrix, angles, plot=False):
    ''' calculate 6 features based on the coocurence matrix '''
    contrast = greycoprops(comatrix, 'contrast')
    dissimilarity = greycoprops(comatrix, 'dissimilarity')
    homogeneity = greycoprops(comatrix, 'homogeneity')
    asm = greycoprops(comatrix, 'ASM')
    energy = greycoprops(comatrix, 'energy')
    correlation = greycoprops(comatrix, 'correlation')

    if plot:
        print('-------')
        print(np.round(contrast, decimals=3))
        print(np.round(dissimilarity, decimals=3))
        print(np.round(homogeneity, decimals=3))
        print(np.round(asm, decimals=3))
        print(np.round(energy, decimals=3))
        print(np.round(correlation, decimals=3))
        print('-------')

    return contrast, dissimilarity, homogeneity, asm, energy, correlation


spots = [(800, 150), (780, 500)]
depth = 8
for spot in spots:
    orig_img, spot_img = read_image('ImgPIA2.jpg', coordinates=spot, size=(100, 100), depth=depth, plot=False)
    spectrum = create_spectrum(spot_img, plot=False)

    values_circlesum = []
    values_linesum = []

    max_radius = int(spectrum.shape[0] / 2)
    for radius in range(0, max_radius, 5):
        circle_sum = get_circle_sum(spectrum, radius, plot=False)
        values_circlesum.append(circle_sum)

    degrees = [0, 45, 90, 135]
    for degree in degrees:
        line_sum = get_line_sum(spectrum, int(degree), width=2, plot=False)
        values_linesum.append(line_sum)

    plt.figure(num='spot_{}x{}'.format(spot[0], spot[1]))
    plt.suptitle('     coord:{}x{}'.format(spot[0], spot[1]))
    plt.subplot(221), plt.imshow(spot_img, cmap='gray')
    plt.subplot(222), plt.imshow(spectrum, cmap='gray')
    plt.subplot(223), plt.plot(range(0, max_radius, 5), values_circlesum)
    plt.xlabel("Radius")
    plt.subplot(224), plt.bar(list(map(str, degrees)), values_linesum)
    plt.xlabel("Degrees")
    plt.tight_layout()
    plt.show()


spot = (800, 150)
for depth in [8, 4, 2]:
    orig_img, spot_img = read_image('ImgPIA2.jpg', coordinates=spot, size=(100, 100), depth=depth, plot=False)
    angles = [0, np.pi / 4, np.pi / 2, 3 * np.pi / 4]
    comatrix_img, comatrix = get_cooccurence(spot_img, angles, plot=False)
    contrast, dissimilarity, homogeneity, asm, energy, correlation = get_textures(comatrix, angles, plot=True)
    hist = get_histogram(spot_img, depth=depth, plot=True)
