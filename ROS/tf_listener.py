#!/usr/bin/env python

import rospy
import tf
import geometry_msgs.msg
from math import atan2, pi


class Transformation():
    def __init__(self, source_frame, target_frame, runForever=True):
        self.runForever = runForever
        self.source_frame = source_frame
        self.target_frame = target_frame

        self.listener = tf.TransformListener()
        self.pose_pub = rospy.Publisher(
            'test_pose', geometry_msgs.msg.PoseStamped, queue_size=1)
        self.rate = rospy.Rate(10.0)

    def lookupTransf(self):
        try:
            # look up the transform, i.e., get the transform from Thorvald_002 to Thorvald_001,
            # This transform will allow us to transfer
            trans, rot = self.listener.lookupTransform(
                self.source_frame, self.target_frame, rospy.Time())
        except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
            trans, rot = None, None
        return trans, rot

    def runCurrentTransf(self):
        found = False
        while not rospy.is_shutdown() and not found:
            trans, rot = self.lookupTransf()
            self.rate.sleep()
            if (trans, rot) != (None, None):
                yaw_angle = atan2(rot[2], rot[3]) * 2
                print("the current transformation: ",
                      trans, rot, yaw_angle * 180 / pi)
                if not self.runForever:
                    found = True

    def runExample(self):
        found = False
        while not rospy.is_shutdown() and not found:
            trans, rot = self.lookupTransf()
            self.rate.sleep()
            if (trans, rot) != (None, None):
                yaw_angle = atan2(rot[2], rot[3]) * 2
                print("the current transformation: ",
                      trans, yaw_angle * 180 / pi)

                # here is an exmaple pose, with a given frame of reference, e.g. somethng detected in the camera
                p1 = geometry_msgs.msg.PoseStamped()
                p1.header.frame_id = "thorvald_002/kinect2_rgb_optical_frame"
                p1.pose.orientation.w = 1.0  # Neutral orientation
                p1.pose.position.z = .5  # half a metre away from the from frame centre
                # we publish this so we can see it in rviz:
                self.pose_pub.publish(p1)

                # here we directly transform the pose into another pose for the given frame of reference:
                p_in_base = self.listener.transformPose(
                    "thorvald_001/base_link", p1)
                print "Position of the object in the new frame of reference:"
                print p_in_base

                self.rate.sleep()
                if not self.runForever:
                    found = True


if __name__ == '__main__':
    rospy.init_node('tf_listener')
    Transformation('thorvald_001/base_link',
                   'thorvald_002/base_link', False).runExample()

    # Transformation('thorvald_001/kinect2_rgb_optical_frame',
    #                'map', False).runCurrentTransf()
