#!/usr/bin/env python

# Python libs
import sys
import time

# OpenCV
import cv2
from cv2 import imshow

# Ros libraries
import roslib
import rospy
import image_geometry

# Ros Messages
from sensor_msgs.msg import Image, CameraInfo
from geometry_msgs.msg import PoseStamped
from cv_bridge import CvBridge, CvBridgeError

import numpy as np


class image_projection:
    camera_model = None

    def __init__(self, robot):
        self.robot = robot

        self.bridge = CvBridge()

        self.camera_info_sub = rospy.Subscriber('/%s/kinect2_camera/hd/camera_info' % (self.robot),
                                                CameraInfo, self.camera_info_callback)

        rospy.Subscriber("/%s/kinect2_camera/hd/image_color_rect" % (self.robot),
                         Image, self.image_callback)

    def image_callback(self, data):
        if not self.camera_model:
            return

        # project a point in camera coordinates into the pixel coordinates
        uv = self.camera_model.project3dToPixel((0, 0, 0.5))

        print 'Pixel coordinates: ', uv
        print ''

        try:
            cv_image = self.bridge.imgmsg_to_cv2(data, "bgr8")
        except CvBridgeError as e:
            print(e)

        # Show Green stuff
        hsv = cv2.cvtColor(cv_image, cv2.COLOR_BGR2HSV)
        lower_green = np.array([20, 100, 20])
        upper_green = np.array([100, 255, 100])
        mask = cv2.inRange(hsv, lower_green, upper_green)
        res = cv2.bitwise_and(cv_image, cv_image, mask=mask)

        # # Contours
        # gray = cv2.cvtColor(res, cv2.COLOR_BGR2GRAY)
        # edged = cv2.Canny(gray, 30, 200)
        # contours, hierarchy = cv2.findContours(edged,
        #                                        cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
        # cv2.drawContours(res, contours, -1, (0, 255, 0), 3)

        # gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        # ret, thresh = cv2.threshold(imgray, 127, 255, 0)
        # contours = cv2.findContours(
        #     thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        # contours, hierarchy = cv2.findContours(edged,
        #                                        cv2.RETR_EXTERNAL,
        #                                        cv2.CHAIN_APPROX_NONE
        #                                        )
        # print(contours)
        # cv2.drawContours(res, contours, -1, (0, 255, 0), 3)

        # # Create bullseye
        # cv2.circle(res, (int(uv[0]), int(uv[1])), 200, 0, -1)
        # cv2.circle(res, (int(uv[0]), int(uv[1])), 150, 100, -1)
        # cv2.circle(res, (int(uv[0]), int(uv[1])), 100, 200, -1)
        # cv2.circle(res, (int(uv[0]), int(uv[1])), 50, 255, -1)
        # cv2.circle(res, (int(uv[0]), int(uv[1])), 10, 0, -1)

        # resize for visualisation
        cv_image_s = cv2.resize(res, (0, 0), fx=0.5, fy=0.5)

        cv2.imshow("Image window", cv_image_s)
        cv2.waitKey(1)

    def camera_info_callback(self, data):
        self.camera_model = image_geometry.PinholeCameraModel()
        self.camera_model.fromCameraInfo(data)
        self.camera_info_sub.unregister()  # Only subscribe once


def main(args):
    '''Initializes and cleanup ros node'''
    rospy.init_node('image_projection', anonymous=True)
    image_projection('thorvald_001')
    # image_projection('thorvald_002')
    try:
        rospy.spin()
    except KeyboardInterrupt:
        print "Shutting down"
    cv2.destroyAllWindows()


if __name__ == '__main__':
    main(sys.argv)
