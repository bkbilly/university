#!/usr/bin/env python
import rospy
from sensor_msgs.msg import LaserScan
from geometry_msgs.msg import Twist


def callback(data):
    print len(data.ranges)
    right = min(data.ranges[0:90])
    middle1 = min(data.ranges[90:360])
    middle2 = min(data.ranges[360:630])
    left = min(data.ranges[630:719])
    print(left, middle1, middle2, right)

    if middle1 < 3:
        move(way='left')
    elif middle2 < 3:
        move(way='right')
    # elif left < 2:
    #     move(way='right')
    # elif right < 2:
    #     move(way='left')
    else:
        move()


def listener():

    # In ROS, nodes are uniquely named. If two nodes with the same
    # name are launched, the previous one is kicked off. The
    # anonymous=True flag means that rospy will choose a unique
    # name for our 'listener' node so that multiple listeners can
    # run simultaneously.

    rospy.Subscriber("/thorvald_002/scan", LaserScan, callback)

    # spin() simply keeps python from exiting until this node is stopped
    rospy.spin()


def move(way='forward'):

    # Starts a new node
    # rospy.init_node('robot_cleaner', anonymous=True)
    velocity_publisher = rospy.Publisher(
        '/thorvald_002/twist_mux/cmd_vel', Twist, queue_size=10)
    vel_msg = Twist()

    vel_msg.linear.x = 0
    vel_msg.linear.y = 0
    vel_msg.linear.z = 0
    vel_msg.angular.x = 0
    vel_msg.angular.y = 0
    vel_msg.angular.z = 0
    if way == 'forward':
        vel_msg.linear.x = 1
    elif way == 'left':
        vel_msg.linear.x = 1
        vel_msg.angular.z = 1
    elif way == 'right':
        vel_msg.linear.x = 1
        vel_msg.angular.z = -1

    velocity_publisher.publish(vel_msg)


if __name__ == '__main__':
    rospy.init_node('scan_values', anonymous=True)
    listener()
