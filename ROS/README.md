# ROS


## Links
 - https://github.com/LCAS/CMP9767M/wiki
 - https://gitter.im/lcas-teaching/CMP9767M
 - https://github.com/LCAS/teaching/tree/kinetic/cmp3103m-code-fragments

## Workspace

### Create New Workspace
```bash
myWorkspace='~/catkin_ws'
source /opt/ros/kinetic/setup.bash
mkdir -p $myWorkspace/src
catkin_make -C $myWorkspace
# Create package
cd $myWorkspace/src
catkin_create_pkg my_package_name rospy -a bkbilly
source $myWorkspace/devel/setup.bash
```
### Create python code
```vi ~/catkin_ws/src/my_package_name/hello.py```

### Run node
To run a node:
```rosrun my_package_name hello.py```
Or to run multiple nodes:
```roslaunch my_package_name LAUNCH_FILE```


## Install dependencies

```bash
sudo apt-get install curl
curl https://raw.githubusercontent.com/LCAS/rosdistro/master/lcas-rosdistro-setup.sh | bash -
sudo apt-get update && sudo apt-get upgrade
sudo apt-get install ros-kinetic-uol-cmp9767m-base ros-kinetic-rviz
sudo apt-get install ros-kinetic-opencv-apps ros-kinetic-rqt-image-view ros-kinetic-find-object-2d ros-kinetic-video-stream-opencv ros-kinetic-topic-tools ros-kinetic-rqt-tf-tree ros-kinetic-image-geometry ros-kinetic-robot-localization
sudo apt-get install python-catkin-tools
rosdep install -y --from-paths src --ignore-src --rosdistro kinetic
```


## Usefull Commands

Run the example:
```bash
# Run ROS example
roslaunch uol_cmp9767m_base thorvald-sim.launch obstacles:=false second_robot:=false
# Run only the Core System
roscore
# Run rviz
rosrun rviz rviz
rviz -d rospack find uol_cmp9767m_base/rviz/two_robots.rviz
# Display the tf tree
rosrun rqt_tf_tree rqt_tf_tree
```
get the description topic of robot for rviz:
```bash
rosparam list | grep -i descr
```
Read a topic
```bash
rostopic echo /thorvald_001/scan
```
List topics
```bash
rostopic list -v
```
Get topics with specific message type
```bash
rostopic find nav_msgs/Odometry
```
Get information about a topic
```bash
rostopic info /thorvald_001/turtle1/cmd_vel
```
Make the robot turn:
```bash
rostopic pub /thorvald_001/turtle1/cmd_vel geometry_msgs/Twist "linear:
  x: 2.0
  y: 0.0
  z: 0.0
angular:
  x: 0.0
  y: 0.0
  z: 2.0" -r 3
```
Display the position of the robot's Kinect camera
```bash
rosrun tf tf_echo thorvald_001/kinect2_rgb_optical_frame thorvald_001/odom
```

RoboVision
```bash
roslaunch video_stream_opencv camera.launch video_stream_provider:=/dev/video0 camera_name:=camera visualize:=true
rosrun opencv_apps simple_flow image:=/camera/image_raw
rosrun opencv_apps find_contours image:=/camera/image_raw
rqt_image_view
```

rostopic hz
roswtf
topic_tools (republish slow topics)

rosservice call /thorvald_001/spray


```bash
rostopic pub /move_base/goal move_base_msgs/MoveBaseActionGoal "header:
  seq: 0
  stamp:
    secs: 0
    nsecs: 0
  frame_id: ''
goal_id:
  stamp:
    secs: 0
    nsecs: 0
  id: ''
goal:
  target_pose:
    header:
      seq: 0
      stamp:
        secs: 0
        nsecs: 0
      frame_id: 'map'
    pose:
      position:
        x: 6
        y: -2.8
        z: 0.0
      orientation:
        x: 0.0
        y: 0.0
        z: 0.0
        w: 1.0" 
```