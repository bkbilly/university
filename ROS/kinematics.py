#!/usr/bin/env python
import rospy
from sensor_msgs.msg import JointState
from nav_msgs.msg import Odometry

class Kinematics():
    def __init__(self):
        self.odom_x = None
        rospy.Subscriber("/joint_states", JointState, self.joint_states)
        rospy.Subscriber("/odom", Odometry, self.odom)
        
    def odom(self, data):
        # print(data.twist.twist.linear.x)
        self.odom_x = data.twist.twist.linear.x

    def joint_states(self, data):
        if self.odom_x is not None:
            wl, wr = data.velocity
            r = 0.076 / 2
            d = 0.150
            v = (self.odom_x / 2) * r * (wl + wr)
            a = (self.odom_x / d) * r * (wl - wr)
            print('v={0:.2f}\na={1:.2f}'.format(v, a))

if __name__ == '__main__':
    rospy.init_node('scan_values', anonymous=True)
    Kinematics()
    rospy.spin()
