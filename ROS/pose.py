#!/usr/bin/env python
import rospy
from sensor_msgs.msg import LaserScan
from geometry_msgs.msg import PoseStamped
import tf


class pose_nearest():
    def __init__(self, robot):
        self.robot = robot
        self.pose_msg = '/%s/geometry_msgs/PoseStamped' % (self.robot)
        rospy.Subscriber("/%s/scan" % (self.robot), LaserScan, self.callback)

    def callback(self, data):
        minvalue = min(data.ranges)
        min_index = data.ranges.index(minvalue)
        angle = data.angle_min + (min_index * data.angle_increment)
        self.pose_angle(angle, minvalue, min_index)
        print(angle, min_index, len(data.ranges), minvalue, max(data.ranges))

    def pose_angle(self, angle, minvalue, min_index):
        camera_pos = '%s/kinect2_depth_optical_frame' % (self.robot)
        robot_pos = '%s/base_link' % (self.robot)
        trans, rot = self.lookupTransf(camera_pos, robot_pos)
        try:
            type(self.pub)
        except:
            self.pub = rospy.Publisher(
                self.pose_msg, PoseStamped, queue_size=1)

        p = PoseStamped()

        p.header.seq = 1
        p.header.stamp = rospy.Time.now()
        p.header.frame_id = robot_pos

        p.pose.position.x = trans[0]
        p.pose.position.y = trans[1]
        p.pose.position.z = trans[2]
        # Make sure the quaternion is valid and normalized
        quaternion = tf.transformations.quaternion_from_euler(0, 0, angle)
        p.pose.orientation.x = 0
        p.pose.orientation.y = 0
        p.pose.orientation.z = quaternion[2]
        p.pose.orientation.w = 1
        print(' - publish to: %s' % (self.pose_msg))
        # # print(quaternion)
        print(p)
        self.pub.publish(p)

    def lookupTransf(self, source, target):
        # Gets the robot position
        try:
            type(self.listener)
        except:
            self.listener = tf.TransformListener()

        found = False
        while not rospy.is_shutdown() and not found:
            try:
                trans, rot = self.listener.lookupTransform(
                    target, source, rospy.Time())
                found = True
            except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
                trans, rot = None, None
        return trans, rot


if __name__ == '__main__':
    rospy.init_node('pose_publisher', anonymous=True)
    pose_nearest('thorvald_001')
    pose_nearest('thorvald_002')
    rospy.spin()
