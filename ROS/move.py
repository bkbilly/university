#!/usr/bin/env python
import rospy
from sensor_msgs.msg import LaserScan
from geometry_msgs.msg import Twist


class moveRobot():
    def __init__(self, robot):
        self.robot = robot
        # In ROS, nodes are uniquely named. If two nodes with the same
        # name are launched, the previous one is kicked off. The
        # anonymous=True flag means that rospy will choose a unique
        # name for our 'listener' node so that multiple listeners can
        # run simultaneously.

        rospy.Subscriber("/%s/scan" % (self.robot), LaserScan, self.callback)

    def callback(self, data):
        right = min(data.ranges[0:90])
        middle1 = min(data.ranges[90:360])
        middle2 = min(data.ranges[360:630])
        left = min(data.ranges[630:719])
        print(self.robot, left, middle1, middle2, right)

        if middle1 < 3 or middle2 < 3:
            if middle1 < middle2:
                self.move(way='left')
            elif middle2 < middle1:
                self.move(way='right')
        # elif left < 2:
        #     self.move(way='right')
        # elif right < 2:
        #     self.move(way='left')
        else:
            self.move()

    def move(self, way='forward'):
        self.velocity_publisher = rospy.Publisher(
            '/%s/twist_mux/cmd_vel' % (self.robot), Twist, queue_size=10)

        # Starts a new node
        # rospy.init_node('robot_cleaner', anonymous=True)
        vel_msg = Twist()

        vel_msg.linear.x = 0
        vel_msg.linear.y = 0
        vel_msg.linear.z = 0
        vel_msg.angular.x = 0
        vel_msg.angular.y = 0
        vel_msg.angular.z = 0
        if way == 'forward':
            vel_msg.linear.x = 1
        elif way == 'left':
            vel_msg.linear.x = 1
            vel_msg.angular.z = 1
        elif way == 'right':
            vel_msg.linear.x = 1
            vel_msg.angular.z = -1

        self.velocity_publisher.publish(vel_msg)


if __name__ == '__main__':
    rospy.init_node('scan_values', anonymous=True)
    moveRobot('thorvald_001')
    moveRobot('thorvald_002')
    # spin() simply keeps python from exiting until this node is stopped
    rospy.spin()
