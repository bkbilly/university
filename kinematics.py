import numpy as np


T1 = 0 # Theta 1 angle in degrees
T2 = 0 # Theta 2 angle in degrees

T1 = (T1/180.0) * np.pi  # Theta 1 in radians
T2 = (T2/180.0) * np.pi  # Theta 2 in radians

R0_1 = [
	[np.cos(T1), -np.sin(T1), 0],
	[np.sin(T1), np.cos(T1), 0],
	[0, 0, 1]
]
R1_2 = [
	[np.cos(T2), -np.sin(T2), 0],
	[np.sin(T2), np.cos(T2), 0],
	[0, 0, 1]
]

R0_2 = np.dot(R0_1, R1_2)

print(R0_2)
