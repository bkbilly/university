#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import csv
import re
from itertools import groupby


observed_data = []
with open('q1-2019-telecoms-data-update.csv') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    title = ''
    table = ''
    comment = ''
    for row in csv_reader:
        foundTable = re.match(r'(Table \d+):?\s*(.*)', row[0])
        foundData = re.match(r'(\d+ Q\d)', row[0])
        if 'Note' in row[0]:
            continue
        elif row[0] == '' and (row[1] != '' or row[2] != ''):
            data_titles = row
            data_titles[0] = 'date'
        elif foundData:
            dictionary = dict(zip(data_titles, row))
            dictionary.pop('', None)
            dictionary['table'] = table
            dictionary['comment'] = comment
            dictionary['title'] = title.lower()
            dictionary['uniq'] = '{0} - {1}: {2}'.format(title, table, comment)
            observed_data.append(dictionary)
            # print(dictionary)
        elif foundTable:
            table = foundTable.group(1)
            comment = foundTable.group(2)
        elif len(filter(None, row)) == 1:
            if row[0].lower() in ['fixed', 'mobile']:
                title = row[0]
            else:
                comment = row[0]
        # else:
        #     print(row)

# asdf = list(set([d['comment'] for d in observed_data]))
# observed_data = [d for d in observed_data if d['title'] == 'mobile']
data_dict = dict((k, list(g)) for k, g in groupby(observed_data, lambda x: x['uniq']))

for key, values in data_dict.items():
    print(len(values), key)
    # asdf = dict((k, list(g)) for k, g in groupby(values, lambda x: x['date']))
    # print(asdf['2016 Q4'])
