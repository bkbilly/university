## Quantitative
can be counted or measured

### Descriptive
intend to describe data with summary charts and tables, etc.
Understanding of underlying reasons, opinions, and motivations, meaning

#### Central Tendency
###### Mean
![equation]( https://latex.codecogs.com/gif.latex?\overline{X}=\frac{sum}{min} )
###### Median
middle value of the ordered data
###### Mode
most frequent value in data set

#### Non-Central Tendency
##### Quartiles
split a set of ordered data into 4 parts
##### Deciles
split a set of ordered data into 10 parts
##### Quintiles
split a set of ordered data into 5 parts
##### Percentiles
split a set of ordered data into 100 parts

#### Dispersion
##### Range
(max-min)
##### Variance
![equation]( https://latex.codecogs.com/gif.latex?s^2=\frac{sum-min}{n-1} )
##### Standard Deviation 
![equation]( https://latex.codecogs.com/gif.latex?s=\sqrt{\frac{\sum_{i=1}^{n}(X_i-\overline{X}%29^2}{n-1}} )
##### Coefficient of Variation
![equation]( https://latex.codecogs.com/gif.latex?CV=\frac{s}{\overline{X}} )


#### Shape
##### Skewness
![equation]( https://latex.codecogs.com/gif.latex?Skewness=\frac{\sum(X-\overline{X}%29^3}{(N-1%29s^3} )

 - positively skewed
 - normal distribution
 - negatively skewed

##### Kurtosis

 - Normal
 - Flat
 - Peaked


## Inferential
### Estimation

 - Target Population
 	- N (Size)
 	- μ (Mean)
 	- σ (Standard deviation)
 - Sample
 	- n (Size)
 	- X (Mean with overline)
 	- S (Standard deviation)


#### Z-score
![equation]( https://latex.codecogs.com/gif.latex?Z_i=\frac{X_i-\overline{X}}{S} )
 OR 
![equation]( https://latex.codecogs.com/gif.latex?Z=\frac{X-\mu}{\sigma} )


#### Central Limit Theorem (CLT)
![equation]( https://latex.codecogs.com/gif.latex?\mu_{\overline{X}}=\mu )

![equation]( https://latex.codecogs.com/gif.latex?\mu_{\overline{X}}=\frac{\sigma}{\sqrt{n}} )



#### Confidence Interval Estimators (CIE)
![equation]( https://latex.codecogs.com/gif.latex?\overline{X}\bar{+}1*\frac{\sigma}{\sqrt{n}}=68\% )

![equation]( https://latex.codecogs.com/gif.latex?\overline{X}\bar{+}2*\frac{\sigma}{\sqrt{n}}=95\% )

![equation]( https://latex.codecogs.com/gif.latex?\overline{X}\bar{+}3*\frac{\sigma}{\sqrt{n}}=99.7\% )


![equation]( https://latex.codecogs.com/gif.latex?confidence=(1-a%29*100\% )

![equation]( https://latex.codecogs.com/gif.latex?a=FromConfidenceTableUsingZvalue )


#### Z-distribution & t-distribution
Use seperate tables based on the distribution.

![equation]( https://latex.codecogs.com/gif.latex?\overline{X}\bar{+}Z_a*\frac{\sigma}{\sqrt{n}} )

![equation]( https://latex.codecogs.com/gif.latex?\overline{X}\bar{+}t_a*\frac{\sigma}{\sqrt{n}} )

|               | Large sample (n) | Small sample (n)  |
|               | ---------------- | ----------------  |
| **σ known**   | Z-distribution   | Z-distribution    |
| **σ unknown** | Z-distribution   | t-distribution*   |

* Normally distributed population

#### Sample Size

![equation]( https://latex.codecogs.com/gif.latex?m=Z_a*\frac{\sigma}{\sqrt{n}} )


### Hypothesis testing
