# Week 1


### Foundations of Robotics
https://www.google.com/search?q=erica+robot&oq=erica&aqs=chrome.1.69i57j0l5.2956j0j7&sourceid=chrome&ie=UTF-8
Math - Metrices, Vectors, Probability and causality

 - All excersizes from chapters 1-4 of Nicholson, Linear Algebra with Applications (this is a Creative Commons textbook which is freely available and downloadable).
 - Make up a 2D coordinate for a mobile agricultural robot, and a 2D coordinate for a cow in the environment. Calculate the position of the cow in the robot's egocentric frame using trigonometry, then using matrices. Repeat for a 3D robot and object.
 - Make up three lengths for the components of a three-element robot arm, with a rotating base. Find the coordinate of the hand in the coordinate system where the base of the arm is the origin, e.g. using rotation matrices.
 - Install Ubuntu 16.04 on your computer, or fnd and learn to use it on a department computer. (essential for next week).
 - The members of SO(2) can be visualized as points on a unit circle how might you visualize or otherwise imagine the shape (topology) of SO(3)?
 - Short essay: `Modern AI robotics should be based on physical simulation rather than logic. Discuss.` e.g. starting point1s:
	https://en.wikipedia.org/wiki/History_of_artifcial_intelligence
	https://en.wikipedia.org/wiki/Embodied_cognition
 - Find 10 interesting robotics jobs on the internet and check if they meet our definition of robotics. What are the current trends in the robotics job market?


**Robot Communication** - 1Software Defned Radio (SDR) is a recent technology which allows a single hardware radio transceiver to act on almost any radio frequency. An SDR board can be used along with open source software GNURadio to control transmit and receive on single frequencies or bands of frequencies. The project as assessment question will explore practical SDR by building and explaining an implementation of a protocol of your group's choice. For example, you could try to control a radio controlled children's toy, commercial robot, or car security key by reverse engineering and re-implementing its protocol.
Example exam question option: "Discuss how radio communications can be used to control a robot, giving detailed examples of hardware and software systems from a real application."

`sudo python -m easy_install --upgrade pyOpenSSL`
`sudo pip install bitstring`
`sudo apt install hackrf`
`sudo apt install rtl-sdr`
`sudo apt install gr-osmosdr`



### Robot Programming
 - https://github.com/LCAS/CMP9767M/wiki/Workshop-1---Introduction-and-ROS-Basics
roscore
 - https://rl.talis.com/3/lincoln/lists/FCC723CB-819A-1C6B-2627-516956C7E48D.html


### Artificial intelligence: a modern approach
Book  by Stuart J. Russell; Peter Norvig  2016  Recommended Reading (`Chapter 1,2`)

### Research Methods
Homework 1

Please investigate the following questions:

 - What is a P‐Value?
 - Why do we need a P‐Value?
	 - When you perform a hypothesis test in statistics, a p‐value helps you determine the significance of your results. The p‐value is a number between 0 and 1 and interpreted in the following way: A small p‐value (typically ≤ 0.05) indicates strong evidence against the null hypothesis, so you reject the null hypothesis.

Homework 2

 - Read the assignment brief and CRG
 - Investigate possible datasets
	 - Something related to your project?
	 - Find an existing dataset,
		 - e.g. https://scholar.google.co.uk/ search for “dataset”
		 - must be referenced in your report
	 - and/or establish the need to collect a new dataset.
		 - see also Lecture 2: Research with Human Participants
		 - and Lecture 3: Design of Surveys and Questionnaires
	 - Analyse which types of data are provided or required
		 - nominal, ordinal or scale?



# Week 2


### Foundations of Robotics
#### Theory

 - Mechanics questions from Open University "are you ready for Electromagnetism" question sheet. If this is too easy then please help other students to develop your team skills. If it is hard, use standard engineering maths textbooks and your colleagues to help.
 	- Q20 (Newton’s second law; uniform acceleration)
 		- A constant force of magnitude 2×10^-5 N acts on a particle of mass 5×10^−6 kg.
 			- (a) Determine the magnitude of the acceleration of theparticle.
 			- (b) If the particle is at rest at time t=0, how far will it have moved by time t=1s?
 	- Q21 (momentum and kinetic energy)
 		- Determine
 			- (a) the magnitude of the momentum, and
 			- (b) the kinetic energy
 		- of a body of mass 3mg moving at a speed of 5cms^−1.
 	- Q22 (force and the gradient of potential energy)
 		- A body moves along the x-axis in a region where its potential energy function is given by V(x)=Cx^2, where C is a constant. Determine the force acting on the body when it is at x=2m, given that C=5Jm^−2.
 	- Q23 (centripetal acceleration, angular momentum, torque)
 		- A body of mass 10^−2kg moves at a constant speed of 2ms^−1 along a circular path of radius 0.5m.
 			- (a) What are the magnitude and direction of the acceleration?
 			- (b) Specify the force acting on the body.
 			- (c) What is the torque of the force about the centre of the circular path?
 			- (d) What is the  magnitude of the angular momentum about the centre?
 - Suppose a 250kg tracked agricultural robot has broken down and needs to be towed back to the farm. It has two tracks about 1m long and 200mm wide. The tracks are stuck and do not move. Estimate the force needed to gain traction to pull it home. What is the smallest commercial tractor you can find able to provide this force? How many horses ( 1 "big horse" power is about 1kW; a traditional "horsepower" is a bit less, around .75kW) is it equivalent to? Suppose the robots themselves are 1 big-horsepower. Can they be used to rescue one another?
 - Using Gazebo tutorials, design and run a simulated robot:
 	-  http://gazebosim.org/tutorials?tut=build_robot&cat=build_robot
 - ~~SOS: Apply torques to the robots wheels in Gazebo. Find settings and experiment with friction on the wheels.~~
 - ~~Extend the basic model to a car-like vehicle where the rear wheels move together at a speed, and the front wheels are coupled by a steering linkage. (Use different joint types to do this).~~
 - ~~Describe each of the fields in the Gazebo XML and how does Gazebo's XML ontology map to the folk physics we have discussed?~~
 - ~~Do as many of the exersizes from LaValle chapter 3 as you can.~~
 - ~~Short essay: Physics engines do not use formal axioms, and make use of apparently arbitrary "folk physics" rules implemented in code. What connection do these simulations have to Physics and to the real world, and why should we trust them to provide predictions of what happens in the real world?~~

### Workshop

 	- https://github.com/on1arf/gr-morsecode-enc
 	- https://github.com/duggabe/gr-morse-code-gen


### Robot Programming

 	- https://github.com/LCAS/CMP9767M/wiki/Workshop-2---ROS-workspaces-and-actual-coding


### Advanced AI
Book  by Stuart J. Russell; Peter Norvig  2016  Recommended Reading (`Chapter 13`)



# Week 3


### Foundations of Robotics
#### Theory

 - Arduino tutorials from the Elegoo kit, see Blackboard download. Do as many as you can from lessons1-7.
 - NB: From some Ubuntus you may errors like "Error opening serial port /dev/ACM0 when trying to connect to the device, which can be fxed by giving your user account permission to access it: "sudo chown <username> /dev/ttyACM0".
 - Take care not to explode the LEDs: LEDs must be connected to gether with a suitable resistor and in the right direction. The longer lego fan LED is the positive end. The diode symbol in the circuit has a bar on the negative end (as it prevents current, rather than electrons, flowing backwards). We will learn more about LEDs and resistors in the next session.



#### Workshop

 - http://www.dspguide.com/pdfbook.htm
 - https://github.com/mossmann/hackrf/wiki/HackRF-One
 - http://aaronscher.com/GNU_Radio_Companion_Collection/Packet_encode_decode.html

### Robot Programming
Do the rest of the exersices: 
https://github.com/LCAS/CMP9767M/wiki/Workshop-3---TFs-and-Sensors


### Advanced AI
 - Implement enumeration, elimination algorithms for the end of semester.
 - Book  by Stuart J. Russell; Peter Norvig  2016  Recommended Reading (`Chapter 14` - `14.4.2`)


# Week 4


### Foundations of Robotics

 - maxwell equations
 - robot wars

 - Electromagnetics questions from Open University "are you ready for Electromagnetism" question sheet; and circuits worksheet. If this is too easy then please help other students to develop your teams kills. If it is hard, use standard engineering maths textbooks and your colleagues to help.
 - Arduino tutorials from Elegoo (last week's blackboard download), lessons 9 (servo motors), 21 (DC motors), 22 (relay), 23 (stepper motor).
 - (optional-advanced) Write a PID controller to control a simulated motor in Gazebo using force applications (as a plugin, e.g. which could receive ROS command messages and apply forces to a joint). Use the Gazebo tutorial to see how to write Gazebo plugins to do this.

### Robot Programming

 - https://github.com/LCAS/CMP9767M/wiki/Workshop-4---Robot-Vision


### Advanced AI
 - Book  by Stuart J. Russell; Peter Norvig  2016  Recommended Reading (`Chapter from 14` - `14.5`)
 - Implement 3 (rejection-sampling, motivation-weighting, gibbs-sampling) algorithms for the end of semester.



# Week 5


### Foundations of Robotics

 - Search the internet or academic literature and makea comparison table of pros and cons of the different 3d ranging sensors. Present your sensor fndings as a written report or presentation. (NB this maybe very examinable).
 - Do as much as you can from Arduino sensor tutorials: Lessons 8 (tilt ball switch), 10 (ultrasonic), 11 (temperature), 12 (joystick), 18 (photo cell).
 - Assessments -> Assessment Documents -> MOCK EXAM
 - rods optics


# Week 6

