# Week 1


## Modern AI robotics should be based on physical simulation rather than logic

A robot needs to learn from the environment with stimuli from the connected sensors with each sensor working together with the moving parts of the robot so that it can learn to adapt to various situations.

At the beginning of Artificial Intelligence, robots where using logic, which made it difficult to complete even the simplest tasks. This was later solved with self learning algorithms which learn from their mistakes, just like humans are processing information. The new type of algorithms are considered to be the physical simulation because they combine the information from the provided sensors and the actions they are making and check the result.

A really important issue with the logic AI is that the programmer can’t take into account every single outcome that might come up to, but using the newest approach helped to cut the cost of development because less lines of code could be used to create a robot like that. 

Many industries have invested a lot of money in the field of AI because of the advantages it brings. There are numerous projects like self driven cars, assistant robots and many more.

Adaptive robots are really important to a real life situation because there are a lot of cases that have not been explored and can be really important to the future of robotics.


## What are the current trends in the robotics job market?

  - Myrmex (Greece) [material handling]
    - ROS, C++/Python, Distributed systems, Machine vision, Visual servoing
  - Spring Professional (Singapore) [implement machine learning algorithms in autonomous system] [3-5 years experience]
    - ROS, C/C++/Python, Windows/Linux
  - Booz Allen Hamilton (US) [sense and navigate through environment] [1 year experience]
    - ROS, C++
  - Tesla (US) [Autonomous Cars]
    - C/C++/Python, Bash, Linux, numpy, scipy, matplotlib, jupyter notebooks, GPS, IMU, radar processing, camera
  - Parkopedia (London) [Autonomus Car Inside Maps]
    - C++/Python, SLAM, ROS, Linux, Git, camera, LiDAR, IMU, GNSS, PCL, Ceres, G20, OpenCV, OpenGV, OpenSfM, Eigen
  - Postmates (US) [3 years] [computer vision, control systems, embedded development, sensor fusion, navigation, and localization]
    - C++/Python/Java, coordinate transformations, kinematics, PID, model predictive controls
  - PSB Recruitment (Switzerland) [SLAM, navigation, and anomaly detection algorithms] [2-3 years]
    - C++, ROS, PCL, Eigen, OpenCV, Boost, Lidar, IMU, 3D Camera

